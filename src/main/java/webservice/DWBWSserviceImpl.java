package webservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.ListsApi;
import io.swagger.client.api.ProjectsApi;
import io.swagger.client.api.TaxonnamesApi;
import io.swagger.client.model.Agentlist;
import io.swagger.client.model.Commonname;
import io.swagger.client.model.Hierarchyfull;
import io.swagger.client.model.Hierarchylink;
import io.swagger.client.model.Link;
import io.swagger.client.model.Listlist;
import io.swagger.client.model.Project;
import io.swagger.client.model.Resultlist;
import io.swagger.client.model.Synonymnamelink;
import io.swagger.client.model.Taxonname;

/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */
public class DWBWSserviceImpl implements WSInterface {
  private static final Logger LOGGER = Logger.getLogger(DWBWSserviceImpl.class);

  private String database, projectId;

  // properties from YAML file
  private WSConfiguration wsConfig;

  public DWBWSserviceImpl(String database, String projectId) {

    initConfig();
    this.database = database;
    this.projectId = projectId;
    LOGGER.info(" a DTNtaxonlists webservice is ready to use");
  }

  public DWBWSserviceImpl(String database) {
    initConfig();
    this.database = database;
    LOGGER.info(" a DTNtaxonlists webservice is ready to use");
  }

  public DWBWSserviceImpl() {
    initConfig();
    LOGGER.info(" a DTNtaxonlists webservice is ready to use");
  }

  private void initConfig() {
    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    wsConfig = reader.getWSConfig("DTNtaxonlists_SNSB");
  }

  @Override
  public String getAcronym() {
    return wsConfig.getAcronym();
  }

  @Override
  public String getDescription() {
    return wsConfig.getDescription();
  }

  @Override
  public String getName() {
    return wsConfig.getName();
  }

  @Override
  public String getURI() {
    return wsConfig.getUri();
  }

  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  @Override
  public String getCurrentVersion() {
    return wsConfig.getVersion();
  }

  private ArrayList<String> getEmbeddedDatabases() {
    ArrayList<String> eD = new ArrayList<String>();
    ListsApi api = new ListsApi();
    try {
      ArrayList<String> seen = new ArrayList<String>();
      for (Listlist list : api.listsGet()) {
        String name = list.getDatabaseName();
        if (!seen.contains(name)) {
          eD.add(name);
          seen.add(name);
        }
      }
    } catch (ApiException e) {
      LOGGER.error("API Exception during querying for embedded Databases.");
    }
    return eD;
  }


  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> search(String query, String match_type) {
    String database, nameid = null;
    TaxonnamesApi api = new TaxonnamesApi();
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("dwb");
    List<Resultlist> taxonList = null;
    try {
      if (match_type.equals(SearchTypes.exact.name())) {
        taxonList = api.namesGet(null, null, null, query);

      } else if (match_type.equals(SearchTypes.included.name())) {
        taxonList = api.namesGet(query, null, null, null);
      }


      for (Resultlist taxonSearchResult : taxonList) {
        String[] cutUrl = taxonSearchResult.getUrl()
            .replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
        database = cutUrl[0];
        nameid = cutUrl[1];
        Taxonname taxonNameResult =
            api.namesDatabaseNameidGet(database, Integer.valueOf(nameid)).get(0);
        SearchResultSetEntry e = new SearchResultSetEntry();
        e.setLabel(taxonSearchResult.getName());
        e.setRank(taxonNameResult.getTaxonomicRank());
        e.setStatus(taxonNameResult.getRevisionLevel());
        e.setUri(taxonSearchResult.getUrl());
        if (!taxonSearchResult.getCommonname().trim().equals("")) {
          ArrayList<String> altName = new ArrayList<String>();
          altName.add(taxonSearchResult.getCommonname().trim());
          e.setCommonNames(altName);
        }
        e.setSourceTerminology(getAcronym());
        e.setEmbeddedDatabase(taxonNameResult.getDatabaseName());
        e.setInternal("false");
        rs.addEntry(e);
      }

      List<Commonname> commonNameList = null;
      commonNameList = api.commonnamesGet(query);

      for (Commonname commonNameSearchResult : commonNameList) {

        if (!commonNameSearchResult.getLinks().isEmpty()) {
          List<Link> links = commonNameSearchResult.getLinks();
          for (Link link : links) {
            String[] cutUrl = link.getUri()
                .replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
            System.out.println(Arrays.toString(cutUrl));
            database = cutUrl[0];
            nameid = cutUrl[1];
            if (nameid.equals(""))
              break;
            Taxonname taxonNameResult =
                api.namesDatabaseNameidGet(database, Integer.parseInt(nameid)).get(0);
            SearchResultSetEntry e = new SearchResultSetEntry();
            e.setLabel(taxonNameResult.getTaxonNameCache());
            e.setRank(taxonNameResult.getTaxonomicRank());
            e.setStatus(taxonNameResult.getRevisionLevel());
            e.setUri(link.getUri());
            ArrayList<String> altName = new ArrayList<String>();
            altName.add(commonNameSearchResult.getCommonName());
            e.setCommonNames(altName);
            e.setSourceTerminology(getAcronym());
            e.setEmbeddedDatabase(taxonNameResult.getDatabaseName());
            rs.addEntry(e);
          }
        }
      }

      return rs;
    } catch (ApiException e) {
      LOGGER.error("Api Exception in DTNtaxonlists during search.");
      return rs;
    }
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original attributes
   * 
   * @return GFBioResultSet
   */
  @Override

  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl =
        term_uri.replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("dwb");
    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    try {
      Taxonname taxonNameResult =
          api.namesDatabaseNameidGet(database, Integer.valueOf(nameid)).get(0);
      e.setOriginalTermInfo("TaxonNameCache", taxonNameResult.getTaxonNameCache());
      e.setOriginalTermInfo("TaxonomicRank", taxonNameResult.getTaxonomicRank());
      e.setOriginalTermInfo("RevisionLevel", taxonNameResult.getRevisionLevel());
      e.setOriginalTermInfo("DatabaseName", taxonNameResult.getDatabaseName());
      e.setOriginalTermInfo("AnamorphTeleomorph", taxonNameResult.getAnamorphTeleomorph());
      e.setOriginalTermInfo("BasionymAuthors", taxonNameResult.getBasionymAuthors());
      e.setOriginalTermInfo("CombiningAuthors", taxonNameResult.getCombiningAuthors());
      e.setOriginalTermInfo("GenusOrSupragenericName",
          taxonNameResult.getGenusOrSupragenericName());
      e.setOriginalTermInfo("IgnoreButKeepForReference",
          taxonNameResult.getIgnoreButKeepForReference());
      e.setOriginalTermInfo("InfragenericEpithet", taxonNameResult.getInfragenericEpithet());
      e.setOriginalTermInfo("InfraspecificEpithet", taxonNameResult.getInfraspecificEpithet());
      e.setOriginalTermInfo("IsRecombination", taxonNameResult.getIsRecombination());
      e.setOriginalTermInfo("Issue", taxonNameResult.getIssue());
      if (taxonNameResult.getNameID() != null) {
        e.setOriginalTermInfo("NameID", taxonNameResult.getNameID().toString());
      }
      e.setOriginalTermInfo("NomenclaturalCode", taxonNameResult.getNomenclaturalCode());
      e.setOriginalTermInfo("NomenclaturalComment", taxonNameResult.getNomenclaturalComment());
      e.setOriginalTermInfo("NomenclaturalStatus", taxonNameResult.getNomenclaturalStatus());
      e.setOriginalTermInfo("NonNomenclaturalNameSuffix",
          taxonNameResult.getNonNomenclaturalNameSuffix());
      e.setOriginalTermInfo("Pages", taxonNameResult.getPages());
      if (taxonNameResult.getDefaultProjectID() != null) {
        e.setOriginalTermInfo("ProjectID", taxonNameResult.getDefaultProjectID().toString());
      }
      e.setOriginalTermInfo("PublishingAuthors", taxonNameResult.getPublishingAuthors());
      e.setOriginalTermInfo("ReferenceTitle", taxonNameResult.getReferenceTitle());
      e.setOriginalTermInfo("ReferenceURI", taxonNameResult.getReferenceURI());
      e.setOriginalTermInfo("SanctioningAuthor", taxonNameResult.getSanctioningAuthor());
      e.setOriginalTermInfo("SpeciesEpithet", taxonNameResult.getSpeciesEpithet());
      e.setOriginalTermInfo("TypistNotes", taxonNameResult.getTypistNotes());
      e.setOriginalTermInfo("Volume", taxonNameResult.getVolume());
      if (taxonNameResult.getVersion() != null) {
        e.setOriginalTermInfo("Version", taxonNameResult.getVersion().toString());
      }
      e.setOriginalTermInfo("Volume", taxonNameResult.getVolume());
      if (taxonNameResult.getYearOfPubl() != null) {
        e.setOriginalTermInfo("YearOfPubl", taxonNameResult.getYearOfPubl().toString());
      }
      List<Synonymnamelink> syn =
          api.namesDatabaseNameidSynonymsGet(database, Integer.valueOf(nameid));
      ArrayList<String> synProjectIDs = new ArrayList<String>();
      for (Synonymnamelink synonym : syn) {
        if (synonym.getProjectID() != null) {
          synProjectIDs.add(synonym.getProjectID().toString());
        }
      }
      e.setOriginalTermInfo("synonymProjectIDs", synProjectIDs);
      List<Hierarchylink> hier =
          api.namesDatabaseNameidHierarchiesGet(database, Integer.valueOf(nameid));
      ArrayList<String> hierProjectIDs = new ArrayList<String>();
      for (Hierarchylink hierarchy : hier) {
        if (hierarchy.getProjectID() != null) {
          hierProjectIDs.add(hierarchy.getProjectID().toString());
        }
      }
      e.setOriginalTermInfo("hierarchyProjectIDs", hierProjectIDs);
      rs.addEntry(e);
      return rs;
    } catch (ApiException ex) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return new GFBioResultSet<TermOriginalResultSetEntry>("dwb");
    }
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original attributes when not
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String term_uri) {
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl =
        term_uri.replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("dwb");
    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    try {
      Taxonname taxonNameResult =
          api.namesDatabaseNameidGet(database, Integer.valueOf(nameid)).get(0);
      e.setLabel(taxonNameResult.getTaxonNameCache());
      e.setRank(taxonNameResult.getTaxonomicRank());
      e.setStatus(taxonNameResult.getRevisionLevel());
      e.setUri(term_uri);
      List<Resultlist> taxonList =
          api.namesGet(null, null, taxonNameResult.getNameID().toString(), null);
      for (Resultlist taxonSearchResult : taxonList) {
        if (taxonSearchResult.getName().equals(taxonNameResult.getTaxonNameCache())) {
          e.setCommonName(taxonSearchResult.getCommonname().trim());
        }
      }
      e.setSourceTerminology(getAcronym());
      e.setEmbeddedDatabase(taxonNameResult.getDatabaseName());
      e.setOriginalTermInfo("AnamorphTeleomorph", taxonNameResult.getAnamorphTeleomorph());
      e.setOriginalTermInfo("BasionymAuthors", taxonNameResult.getBasionymAuthors());
      e.setOriginalTermInfo("CombiningAuthors", taxonNameResult.getCombiningAuthors());
      e.setOriginalTermInfo("GenusOrSupragenericName",
          taxonNameResult.getGenusOrSupragenericName());
      e.setOriginalTermInfo("IgnoreButKeepForReference",
          taxonNameResult.getIgnoreButKeepForReference());
      e.setOriginalTermInfo("InfragenericEpithet", taxonNameResult.getInfragenericEpithet());
      e.setOriginalTermInfo("InfraspecificEpithet", taxonNameResult.getInfraspecificEpithet());
      e.setOriginalTermInfo("IsRecombination", taxonNameResult.getIsRecombination());
      e.setOriginalTermInfo("Issue", taxonNameResult.getIssue());
      if (taxonNameResult.getNameID() != null) {
        e.setOriginalTermInfo("NameID", taxonNameResult.getNameID().toString());
      }
      e.setOriginalTermInfo("NomenclaturalCode", taxonNameResult.getNomenclaturalCode());
      e.setOriginalTermInfo("NomenclaturalComment", taxonNameResult.getNomenclaturalComment());
      e.setOriginalTermInfo("NomenclaturalStatus", taxonNameResult.getNomenclaturalStatus());
      e.setOriginalTermInfo("NonNomenclaturalNameSuffix",
          taxonNameResult.getNonNomenclaturalNameSuffix());
      e.setOriginalTermInfo("Pages", taxonNameResult.getPages());
      if (taxonNameResult.getDefaultProjectID() != null) {
        e.setOriginalTermInfo("ProjectID", taxonNameResult.getDefaultProjectID().toString());
      }
      e.setOriginalTermInfo("PublishingAuthors", taxonNameResult.getPublishingAuthors());
      e.setOriginalTermInfo("ReferenceTitle", taxonNameResult.getReferenceTitle());
      e.setOriginalTermInfo("ReferenceURI", taxonNameResult.getReferenceURI());
      e.setOriginalTermInfo("SanctioningAuthor", taxonNameResult.getSanctioningAuthor());
      e.setOriginalTermInfo("SpeciesEpithet", taxonNameResult.getSpeciesEpithet());
      e.setOriginalTermInfo("TypistNotes", taxonNameResult.getTypistNotes());
      e.setOriginalTermInfo("Volume", taxonNameResult.getVolume());
      if (taxonNameResult.getVersion() != null) {
        e.setOriginalTermInfo("Version", taxonNameResult.getVersion().toString());
      }
      e.setOriginalTermInfo("Volume", taxonNameResult.getVolume());
      if (taxonNameResult.getYearOfPubl() != null) {
        e.setOriginalTermInfo("YearOfPubl", taxonNameResult.getYearOfPubl().toString());
      }
      List<Synonymnamelink> syn =
          api.namesDatabaseNameidSynonymsGet(database, Integer.valueOf(nameid));
      ArrayList<String> synProjectIDs = new ArrayList<String>();
      for (Synonymnamelink synonym : syn) {
        if (synonym.getProjectID() != null) {
          synProjectIDs.add(synonym.getProjectID().toString());
        }
      }
      e.setOriginalTermInfo("synonymProjectIDs", synProjectIDs);
      List<Hierarchylink> hier =
          api.namesDatabaseNameidHierarchiesGet(database, Integer.valueOf(nameid));
      ArrayList<String> hierProjectIDs = new ArrayList<String>();
      for (Hierarchylink hierarchy : hier) {
        if (hierarchy.getProjectID() != null) {
          hierProjectIDs.add(hierarchy.getProjectID().toString());
        }
      }
      e.setOriginalTermInfo("hierarchyProjectIDs", hierProjectIDs);
      rs.addEntry(e);
      return rs;
    } catch (ApiException ex) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return new GFBioResultSet<TermCombinedResultSetEntry>("dwb");
    }
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl =
        term_uri.replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("dwb");
    SearchResultSetEntry e = new SearchResultSetEntry();

    try {
      Taxonname taxonNameResult =
          api.namesDatabaseNameidGet(database, Integer.valueOf(nameid)).get(0);
      e.setLabel(taxonNameResult.getTaxonNameCache());
      e.setRank(taxonNameResult.getTaxonomicRank());
      e.setStatus(taxonNameResult.getRevisionLevel());
      e.setUri(term_uri);
      e.setSourceTerminology(getAcronym());
      e.setEmbeddedDatabase(taxonNameResult.getDatabaseName());
      List<Resultlist> taxonList =
          api.namesGet(null, null, taxonNameResult.getNameID().toString(), null);
      for (Resultlist taxonSearchResult : taxonList) {
        if (taxonSearchResult.getName().equals(taxonNameResult.getTaxonNameCache())) {
          e.setCommonName(taxonSearchResult.getCommonname());
        }
      }
      rs.addEntry(e);
      return rs;
    } catch (ApiException ex) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return new GFBioResultSet<SearchResultSetEntry>("dwb");
    }
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String term_uriORexternalID) {
    GFBioResultSet<HierarchyResultSetEntry> rs = new GFBioResultSet<HierarchyResultSetEntry>("dwb");
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl = term_uriORexternalID
        .replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    try {
      List<Hierarchyfull> hierarchy = api.hierarchyDatabaseidProjectidNameidFullGet(database,
          Integer.valueOf(projectId), Integer.parseInt(nameid));
      for (Hierarchyfull hf : hierarchy) {
        HierarchyResultSetEntry e = new HierarchyResultSetEntry();
        e.setLabel(hf.getTaxonNameCache());
        e.setRank(hf.getTaxonomicRank());
        e.setUri("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/" + database + "/"
            + hf.getNameID() + "/");
        ArrayList<String> arrayl = new ArrayList<String>();
        arrayl.add(hf.getLinks().get(0).getUri());
        e.setHierarchy(arrayl);
        rs.addEntry(e);
      }
    } catch (ApiException e) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return rs;
    }
    return rs;
  }

  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String term_uriORexternalID) {
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("dwb");
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl = term_uriORexternalID
        .replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    try {
      List<Hierarchyfull> hierarchy = api.hierarchyDatabaseidProjectidNameidFullGet(database,
          Integer.parseInt(projectId), Integer.parseInt(nameid));
      hierarchy.remove(0);
      for (Hierarchyfull hf : hierarchy) {
        AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
        e.setRank(hf.getTaxonomicRank());
        e.setLabel(hf.getTaxonNameCache());
        e.setUri("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/" + database + "/"
            + hf.getNameID() + "/");
        rs.addEntry(e);
      }
      return rs;
    } catch (ApiException e) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return rs;
    }
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String term_uriORexternalID) {
    TaxonnamesApi api = new TaxonnamesApi();
    String[] cutUrl = term_uriORexternalID
        .replace("http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/", "").split("/");
    String nameid = cutUrl.length > 1 ? cutUrl[1] : cutUrl[0];
    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("dwb");
    try {
      List<Synonymnamelink> syn =
          api.namesDatabaseNameidSynonymsGet(database, Integer.parseInt(nameid));
      ArrayList<String> synonyms = new ArrayList<String>();
      for (Synonymnamelink synonym : syn) {
        if (synonym.getProjectID() == (Integer.parseInt(projectId))) {
          List<Taxonname> taxonNameResult = api.namesDatabaseNameidGet(synonym.getDatabaseName(),
              Integer.parseInt(synonym.getSynNameID().toString()));
          synonyms.add(taxonNameResult.get(0).getTaxonNameCache());
        }
      }
      SynonymsResultSetEntry syns = new SynonymsResultSetEntry();
      syns.setSynonyms(synonyms);
      rs.addEntry(syns);
    } catch (ApiException e) {
      LOGGER.error("api exception in DTNtaxonlists search");
      return new GFBioResultSet<SynonymsResultSetEntry>("dwb");
    }
    return rs;
  }

  @Override
  public boolean supportsMatchType(String match_type) {
    return Arrays.stream(wsConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(match_type)::equals);
  }

  @Override
  public boolean isResponding() {
    ApiClient client = new ApiClient();
    // test with 30s timeout
    client.setConnectTimeout(10000);
    client.setReadTimeout(10000);


    ListsApi api = new ListsApi(client);
    try {
      api.listsGet();
    } catch (Exception e) {
      return false;
    }

    return true;
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * webservice
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("dwb");
    if (database != null) {
      try {
        ListsApi api = new ListsApi();
        ProjectsApi prapi = new ProjectsApi();
        for (Listlist list : api.listsGet()) {
          if (!list.getDatabaseName().equals(database)) {
            continue;
          }
          MetadataResultSetEntry e = new MetadataResultSetEntry();
          Project pro = prapi.projectsProjectidGet(list.getProjectid()).get(0);
          e.setOriginalTermInfo("Project", pro.getProject());
          e.setOriginalTermInfo("ProjectCopyright", pro.getProjectCopyright());
          e.setOriginalTermInfo("ProjectDescription", pro.getProjectDescription());
          e.setOriginalTermInfo("ProjectEditors", pro.getProjectEditors());
          if (pro.getProjectID() != null) {
            e.setOriginalTermInfo("ProjectID", pro.getProjectID().toString());
          }
          e.setOriginalTermInfo("ProjectInstitution", pro.getProjectInstitution());
          e.setOriginalTermInfo("ProjectLicenseURI", pro.getProjectLicenseURI());
          e.setOriginalTermInfo("ProjectNotes", pro.getProjectNotes());
          if (pro.getProjectParentID() != null) {
            e.setOriginalTermInfo("ProjectParentID", pro.getProjectParentID().toString());
          }
          e.setOriginalTermInfo("ProjectRights", pro.getProjectRights());
          e.setOriginalTermInfo("ProjectSettings", pro.getProjectSettings());
          e.setOriginalTermInfo("ProjectTitle", pro.getProjectTitle());
          e.setOriginalTermInfo("ProjectURL", pro.getProjectURL());
          List<Agentlist> al = prapi.projectsProjectidAgentsGet(list.getProjectid());
          ArrayList<String> agents = new ArrayList<String>();
          for (Agentlist a : al) {
            StringBuilder out = new StringBuilder();
            out.append(a.getAgentName().replace(",", ""));
            out.append(", ");
            out.append(a.getAgentRole());
            out.append(", ");
            out.append(a.getLinks().get(0).getUri());
            agents.add(out.toString());
          }
          e.setOriginalTermInfo("ProjectAgents", agents);
          rs.addEntry(e);
        }
      } catch (ApiException e1) {
        LOGGER.error("Api exception while querying metadata");
      }
    } else {
      MetadataResultSetEntry e = new MetadataResultSetEntry();
      e.setName(wsConfig.getName());
      e.setAcronym(wsConfig.getAcronym());
      e.setVersion(wsConfig.getVersion());
      e.setDescription(wsConfig.getDescription());
      e.setKeywords(wsConfig.getKeywords());
      e.setUri(wsConfig.getUri());
      e.setContact(wsConfig.getContact());
      e.setLanguage(wsConfig.getLanguages());
      e.setCreators(wsConfig.getCreators());
      e.setContributors(wsConfig.getContributors());
      e.setEmbeddedDatabase(getEmbeddedDatabases());
      e.setContribution(wsConfig.getContribution());
      e.setStorage(wsConfig.getStorage());
      for (Domains d : wsConfig.getWsDomains()) {
        e.setDomain(d.getUri());
      }
      rs.addEntry(e);
    }
    return rs;
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("dwb");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : wsConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : wsConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }
}
