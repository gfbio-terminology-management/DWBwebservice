package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.model.Agent;
import io.swagger.client.model.Error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")
public class AgentsApi {
  private ApiClient apiClient;

  public AgentsApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AgentsApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * description of this agents
   * An array one item, describing the agent
   * @param agentid server unique id of the agent (required)
   * @return List&lt;Agent&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Agent> agentsTNTAgentidGet(Integer agentid) throws ApiException {
    return agentsTNTAgentidGetWithHttpInfo(agentid).getData();
      }

  /**
   * description of this agents
   * An array one item, describing the agent
   * @param agentid server unique id of the agent (required)
   * @return ApiResponse&lt;List&lt;Agent&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Agent>> agentsTNTAgentidGetWithHttpInfo(Integer agentid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'agentid' is set
    if (agentid == null) {
      throw new ApiException(400, "Missing the required parameter 'agentid' when calling agentsTNTAgentidGet");
    }
    
    // create path and map variables
    String localVarPath = "/Agents_TNT/{agentid}"
      .replaceAll("\\{" + "agentid" + "\\}", apiClient.escapeString(agentid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Agent>> localVarReturnType = new GenericType<List<Agent>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
