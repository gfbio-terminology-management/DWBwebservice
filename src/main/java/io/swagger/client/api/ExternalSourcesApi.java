package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import javax.ws.rs.core.GenericType;

import java.math.BigDecimal;
import io.swagger.client.model.Error;
import io.swagger.client.model.ExternalNameSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")
public class ExternalSourcesApi {
  private ApiClient apiClient;

  public ExternalSourcesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ExternalSourcesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * links to the external name services
   * get a list of links to external name services. These can be webservices or simple websites with the possibility to link directly to taxonomic name information
   * @param database database name (required)
   * @param externalsourceid database unique id of the external source (required)
   * @return List&lt;ExternalNameSource&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ExternalNameSource> externalnameserviceDatabaseExternalsourceidGet(String database, BigDecimal externalsourceid) throws ApiException {
    return externalnameserviceDatabaseExternalsourceidGetWithHttpInfo(database, externalsourceid).getData();
      }

  /**
   * links to the external name services
   * get a list of links to external name services. These can be webservices or simple websites with the possibility to link directly to taxonomic name information
   * @param database database name (required)
   * @param externalsourceid database unique id of the external source (required)
   * @return ApiResponse&lt;List&lt;ExternalNameSource&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ExternalNameSource>> externalnameserviceDatabaseExternalsourceidGetWithHttpInfo(String database, BigDecimal externalsourceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling externalnameserviceDatabaseExternalsourceidGet");
    }
    
    // verify the required parameter 'externalsourceid' is set
    if (externalsourceid == null) {
      throw new ApiException(400, "Missing the required parameter 'externalsourceid' when calling externalnameserviceDatabaseExternalsourceidGet");
    }
    
    // create path and map variables
    String localVarPath = "/externalnameservice/{database}/{externalsourceid}"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "externalsourceid" + "\\}", apiClient.escapeString(externalsourceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ExternalNameSource>> localVarReturnType = new GenericType<List<ExternalNameSource>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
