package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.model.Agentlist;
import io.swagger.client.model.Error;
import io.swagger.client.model.Project;
import io.swagger.client.model.Referencelink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")
public class ProjectsApi {
  private ApiClient apiClient;

  public ProjectsApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ProjectsApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * returns null
   * Links to all projects on this server. Not implemented.
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String projectsGet() throws ApiException {
    return projectsGetWithHttpInfo().getData();
      }

  /**
   * returns null
   * Links to all projects on this server. Not implemented.
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> projectsGetWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/projects";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<String> localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * all rolls for this agent in this project
   * An array with all rolls which the given agents has in this project
   * @param projectid server unique id of the project (required)
   * @param agenthash an internal identifier of the agent in this project (required)
   * @return List&lt;Agentlist&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Agentlist> projectsProjectidAgentsAgenthashGet(Integer projectid, String agenthash) throws ApiException {
    return projectsProjectidAgentsAgenthashGetWithHttpInfo(projectid, agenthash).getData();
      }

  /**
   * all rolls for this agent in this project
   * An array with all rolls which the given agents has in this project
   * @param projectid server unique id of the project (required)
   * @param agenthash an internal identifier of the agent in this project (required)
   * @return ApiResponse&lt;List&lt;Agentlist&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Agentlist>> projectsProjectidAgentsAgenthashGetWithHttpInfo(Integer projectid, String agenthash) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling projectsProjectidAgentsAgenthashGet");
    }
    
    // verify the required parameter 'agenthash' is set
    if (agenthash == null) {
      throw new ApiException(400, "Missing the required parameter 'agenthash' when calling projectsProjectidAgentsAgenthashGet");
    }
    
    // create path and map variables
    String localVarPath = "/projects/{projectid}/agents/{agenthash}"
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
      .replaceAll("\\{" + "agenthash" + "\\}", apiClient.escapeString(agenthash.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Agentlist>> localVarReturnType = new GenericType<List<Agentlist>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * all agents for this project
   * An array with links to all agents of this project
   * @param projectid server unique id of the project (required)
   * @return List&lt;Agentlist&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Agentlist> projectsProjectidAgentsGet(Integer projectid) throws ApiException {
    return projectsProjectidAgentsGetWithHttpInfo(projectid).getData();
      }

  /**
   * all agents for this project
   * An array with links to all agents of this project
   * @param projectid server unique id of the project (required)
   * @return ApiResponse&lt;List&lt;Agentlist&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Agentlist>> projectsProjectidAgentsGetWithHttpInfo(Integer projectid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling projectsProjectidAgentsGet");
    }
    
    // create path and map variables
    String localVarPath = "/projects/{projectid}/agents"
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Agentlist>> localVarReturnType = new GenericType<List<Agentlist>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * a specific project description on this server
   * An array with one element which represents the description of the selected project
   * @param projectid server unique id of the project (required)
   * @return List&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Project> projectsProjectidGet(Integer projectid) throws ApiException {
    return projectsProjectidGetWithHttpInfo(projectid).getData();
      }

  /**
   * a specific project description on this server
   * An array with one element which represents the description of the selected project
   * @param projectid server unique id of the project (required)
   * @return ApiResponse&lt;List&lt;Project&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Project>> projectsProjectidGetWithHttpInfo(Integer projectid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling projectsProjectidGet");
    }
    
    // create path and map variables
    String localVarPath = "/projects/{projectid}"
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Project>> localVarReturnType = new GenericType<List<Project>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * literature references
   * An array with links to all references of this project
   * @param projectid server unique id of the project (required)
   * @return List&lt;Referencelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referencelink> projectsProjectidReferencesGet(Integer projectid) throws ApiException {
    return projectsProjectidReferencesGetWithHttpInfo(projectid).getData();
      }

  /**
   * literature references
   * An array with links to all references of this project
   * @param projectid server unique id of the project (required)
   * @return ApiResponse&lt;List&lt;Referencelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referencelink>> projectsProjectidReferencesGetWithHttpInfo(Integer projectid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling projectsProjectidReferencesGet");
    }
    
    // create path and map variables
    String localVarPath = "/projects/{projectid}/references"
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referencelink>> localVarReturnType = new GenericType<List<Referencelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
