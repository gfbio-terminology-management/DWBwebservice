package io.swagger.client.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.GenericType;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.model.ListAnalysisCategorieslink;
import io.swagger.client.model.ListAnalysisNames;
import io.swagger.client.model.Listlist;
import io.swagger.client.model.Projectlink;
import io.swagger.client.model.Taxonnamelink;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen",
    date = "2020-11-10T08:47:07.898Z")
public class ListsApi {
  private ApiClient apiClient;

  public ListsApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ListsApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * link to all list items with the given analysis category &lt;p&gt;A list with all list items
   * associated with this analysis id, optinal limiting query parameters can be
   * provided.&lt;/p&gt;&lt;p&gt;The parameters can limit the resultset to a specific
   * TaxonNameListReferenceID which is otherwise ignored.&lt;/p&gt;&lt;p&gt;The operator and the
   * value parameters represents simple SQL-like result limiting possiblity&lt;/p&gt;
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @param analysisid database unique id of the analysis category (required)
   * @param value analysis value to seach for (optional)
   * @param op comparison operator for the seach (eq, lt, gt, le, ge, ne, like, notlike) (optional,
   *        default to =)
   * @param refid optional reference id to limit the results to a specific TaxonNameListReferenceID
   *        (optional, default to -1)
   * @return List&lt;ListAnalysisNames&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ListAnalysisNames> listsDatabaseListidAnalysisAnalysisidGet(String database,
      Integer listid, Integer analysisid, String value, String op, Integer refid)
      throws ApiException {
    return listsDatabaseListidAnalysisAnalysisidGetWithHttpInfo(database, listid, analysisid, value,
        op, refid).getData();
  }

  /**
   * link to all list items with the given analysis category &lt;p&gt;A list with all list items
   * associated with this analysis id, optinal limiting query parameters can be
   * provided.&lt;/p&gt;&lt;p&gt;The parameters can limit the resultset to a specific
   * TaxonNameListReferenceID which is otherwise ignored.&lt;/p&gt;&lt;p&gt;The operator and the
   * value parameters represents simple SQL-like result limiting possiblity&lt;/p&gt;
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @param analysisid database unique id of the analysis category (required)
   * @param value analysis value to seach for (optional)
   * @param op comparison operator for the seach (eq, lt, gt, le, ge, ne, like, notlike) (optional,
   *        default to =)
   * @param refid optional reference id to limit the results to a specific TaxonNameListReferenceID
   *        (optional, default to -1)
   * @return ApiResponse&lt;List&lt;ListAnalysisNames&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ListAnalysisNames>> listsDatabaseListidAnalysisAnalysisidGetWithHttpInfo(
      String database, Integer listid, Integer analysisid, String value, String op, Integer refid)
      throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400,
          "Missing the required parameter 'database' when calling listsDatabaseListidAnalysisAnalysisidGet");
    }

    // verify the required parameter 'listid' is set
    if (listid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'listid' when calling listsDatabaseListidAnalysisAnalysisidGet");
    }

    // verify the required parameter 'analysisid' is set
    if (analysisid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analysisid' when calling listsDatabaseListidAnalysisAnalysisidGet");
    }

    // create path and map variables
    String localVarPath = "/lists/{database}/{listid}/analysis/{analysisid}"
        .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
        .replaceAll("\\{" + "listid" + "\\}", apiClient.escapeString(listid.toString()))
        .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "value", value));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "op", op));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "refid", refid));



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<ListAnalysisNames>> localVarReturnType =
        new GenericType<List<ListAnalysisNames>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * link to analysisids A list with all analysis categories associated with this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return List&lt;ListAnalysisCategorieslink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ListAnalysisCategorieslink> listsDatabaseListidAnalysisGet(String database,
      Integer listid) throws ApiException {
    return listsDatabaseListidAnalysisGetWithHttpInfo(database, listid).getData();
  }

  /**
   * link to analysisids A list with all analysis categories associated with this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return ApiResponse&lt;List&lt;ListAnalysisCategorieslink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ListAnalysisCategorieslink>> listsDatabaseListidAnalysisGetWithHttpInfo(
      String database, Integer listid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400,
          "Missing the required parameter 'database' when calling listsDatabaseListidAnalysisGet");
    }

    // verify the required parameter 'listid' is set
    if (listid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'listid' when calling listsDatabaseListidAnalysisGet");
    }

    // create path and map variables
    String localVarPath = "/lists/{database}/{listid}/analysis"
        .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
        .replaceAll("\\{" + "listid" + "\\}", apiClient.escapeString(listid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<ListAnalysisCategorieslink>> localVarReturnType =
        new GenericType<List<ListAnalysisCategorieslink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }
  /**
   * link to the analysis cetegories in this list which indirectly have a reference to the given
   * referenceid &lt;p&gt;Returns all analysis categories which (indirectly) have a link to the
   * given referenceid.&lt;/p&gt;&lt;p&gt;This means that a parent analysis categorie might be
   * pointing to this referenceid or to a child of this referenceid.&lt;/p&gt;&lt;p&gt;Example:
   * &lt;a
   * href&#x3D;&#39;/lists/DiversityTaxonNames_Insecta/863/analysisfromreference/699074100&#39;&gt;All
   * analysis categories which indirectly refrencing the given Rote Liste
   * publications&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;This indirection over two hierarchies was necessary
   * as normaly only a parent analysis categorie is linked to a referenceid and those referenceids
   * are normaly only books of a series. With this implementation all analysis categories of a
   * complete series of books for this list can be retrieved.&lt;/p&gt;
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;AnalysisCategories&gt;
   * @throws ApiException if fails to make API call
   */
  // public List<AnalysisCategories> listsDatabaseListidAnalysisfromreferenceReferenceidGet(String
  // database, Integer listid, Integer referenceid) throws ApiException {
  // return listsDatabaseListidAnalysisfromreferenceReferenceidGetWithHttpInfo(database, listid,
  // referenceid).getData();
  // }

  /**
   * link to the analysis cetegories in this list which indirectly have a reference to the given
   * referenceid &lt;p&gt;Returns all analysis categories which (indirectly) have a link to the
   * given referenceid.&lt;/p&gt;&lt;p&gt;This means that a parent analysis categorie might be
   * pointing to this referenceid or to a child of this referenceid.&lt;/p&gt;&lt;p&gt;Example:
   * &lt;a
   * href&#x3D;&#39;/lists/DiversityTaxonNames_Insecta/863/analysisfromreference/699074100&#39;&gt;All
   * analysis categories which indirectly refrencing the given Rote Liste
   * publications&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;This indirection over two hierarchies was necessary
   * as normaly only a parent analysis categorie is linked to a referenceid and those referenceids
   * are normaly only books of a series. With this implementation all analysis categories of a
   * complete series of books for this list can be retrieved.&lt;/p&gt;
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategories&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  // public ApiResponse<List<AnalysisCategories>>
  // listsDatabaseListidAnalysisfromreferenceReferenceidGetWithHttpInfo(String database, Integer
  // listid, Integer referenceid) throws ApiException {
  // Object localVarPostBody = null;
  //
  // // verify the required parameter 'database' is set
  // if (database == null) {
  // throw new ApiException(400, "Missing the required parameter 'database' when calling
  // listsDatabaseListidAnalysisfromreferenceReferenceidGet");
  // }
  //
  // // verify the required parameter 'listid' is set
  // if (listid == null) {
  // throw new ApiException(400, "Missing the required parameter 'listid' when calling
  // listsDatabaseListidAnalysisfromreferenceReferenceidGet");
  // }
  //
  // // verify the required parameter 'referenceid' is set
  // if (referenceid == null) {
  // throw new ApiException(400, "Missing the required parameter 'referenceid' when calling
  // listsDatabaseListidAnalysisfromreferenceReferenceidGet");
  // }
  //
  // // create path and map variables
  // String localVarPath = "/lists/{database}/{listid}/analysisfromreference/{referenceid}"
  // .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
  // .replaceAll("\\{" + "listid" + "\\}", apiClient.escapeString(listid.toString()))
  // .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));
  //
  // // query params
  // List<Pair> localVarQueryParams = new ArrayList<Pair>();
  // Map<String, String> localVarHeaderParams = new HashMap<String, String>();
  // Map<String, Object> localVarFormParams = new HashMap<String, Object>();
  //
  //
  //
  //
  // final String[] localVarAccepts = {
  // "application/json"
  // };
  // final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
  //
  // final String[] localVarContentTypes = {
  //
  // };
  // final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
  //
  // String[] localVarAuthNames = new String[] { };
  //
  // GenericType<List<AnalysisCategories>> localVarReturnType = new
  // GenericType<List<AnalysisCategories>>() {};
  // return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
  // localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
  // localVarAuthNames, localVarReturnType);
  // }
  /**
   * links to all taxonomic names in this specific list List of links to taxonomic names which are
   * stored in this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return List&lt;Taxonnamelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Taxonnamelink> listsDatabaseListidGet(String database, Integer listid)
      throws ApiException {
    return listsDatabaseListidGetWithHttpInfo(database, listid).getData();
  }

  /**
   * links to all taxonomic names in this specific list List of links to taxonomic names which are
   * stored in this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return ApiResponse&lt;List&lt;Taxonnamelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Taxonnamelink>> listsDatabaseListidGetWithHttpInfo(String database,
      Integer listid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400,
          "Missing the required parameter 'database' when calling listsDatabaseListidGet");
    }

    // verify the required parameter 'listid' is set
    if (listid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'listid' when calling listsDatabaseListidGet");
    }

    // create path and map variables
    String localVarPath = "/lists/{database}/{listid}"
        .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
        .replaceAll("\\{" + "listid" + "\\}", apiClient.escapeString(listid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<Taxonnamelink>> localVarReturnType = new GenericType<List<Taxonnamelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * link to the project A list with one link to the project which defines this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return List&lt;Projectlink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Projectlink> listsDatabaseListidProjectGet(String database, Integer listid)
      throws ApiException {
    return listsDatabaseListidProjectGetWithHttpInfo(database, listid).getData();
  }

  /**
   * link to the project A list with one link to the project which defines this list
   * 
   * @param database database name (required)
   * @param listid database unique id of the list (required)
   * @return ApiResponse&lt;List&lt;Projectlink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Projectlink>> listsDatabaseListidProjectGetWithHttpInfo(String database,
      Integer listid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400,
          "Missing the required parameter 'database' when calling listsDatabaseListidProjectGet");
    }

    // verify the required parameter 'listid' is set
    if (listid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'listid' when calling listsDatabaseListidProjectGet");
    }

    // create path and map variables
    String localVarPath = "/lists/{database}/{listid}/project"
        .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
        .replaceAll("\\{" + "listid" + "\\}", apiClient.escapeString(listid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<Projectlink>> localVarReturnType = new GenericType<List<Projectlink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * Taxonomic name lists Links to all lists on this server
   * 
   * @return List&lt;Listlist&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Listlist> listsGet() throws ApiException {
    return listsGetWithHttpInfo().getData();
  }

  /**
   * Taxonomic name lists Links to all lists on this server
   * 
   * @return ApiResponse&lt;List&lt;Listlist&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Listlist>> listsGetWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/lists";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<Listlist>> localVarReturnType = new GenericType<List<Listlist>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }
}
