package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.model.Acceptedname;
import io.swagger.client.model.Acceptednamelink;
import io.swagger.client.model.Acceptednameresult;
import java.math.BigDecimal;
import io.swagger.client.model.Commonname;
import io.swagger.client.model.Commonnamelink;
import io.swagger.client.model.Error;
import io.swagger.client.model.ExternalIdentifiers;
import io.swagger.client.model.Hierarchy;
import io.swagger.client.model.Hierarchyfull;
import io.swagger.client.model.Hierarchylink;
import io.swagger.client.model.Listslink;
import io.swagger.client.model.Resultlist;
import io.swagger.client.model.Synonymname;
import io.swagger.client.model.Synonymnamelink;
import io.swagger.client.model.Taxonname;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")
public class TaxonnamesApi {
  private ApiClient apiClient;

  public TaxonnamesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TaxonnamesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * the accepted name
   * An array with one element containing the accepted name dataset
   * @param databaseid database name (required)
   * @param projectid database unique id of the project in which this name is defined as accepted (required)
   * @param nameid database unique id of the name (required)
   * @return List&lt;Acceptedname&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Acceptedname> acceptednamesDatabaseidProjectidNameidGet(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    return acceptednamesDatabaseidProjectidNameidGetWithHttpInfo(databaseid, projectid, nameid).getData();
      }

  /**
   * the accepted name
   * An array with one element containing the accepted name dataset
   * @param databaseid database name (required)
   * @param projectid database unique id of the project in which this name is defined as accepted (required)
   * @param nameid database unique id of the name (required)
   * @return ApiResponse&lt;List&lt;Acceptedname&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Acceptedname>> acceptednamesDatabaseidProjectidNameidGetWithHttpInfo(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling acceptednamesDatabaseidProjectidNameidGet");
    }
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling acceptednamesDatabaseidProjectidNameidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling acceptednamesDatabaseidProjectidNameidGet");
    }
    
    // create path and map variables
    String localVarPath = "/acceptednames/{databaseid}/{projectid}/{nameid}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Acceptedname>> localVarReturnType = new GenericType<List<Acceptedname>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * the common name dataset
   * An array with one element containing the commonname dataset
   * @param databaseid database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @param cid database unique id of the common name, URL-encoded (required)
   * @return List&lt;Commonname&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Commonname> commonnamesDatabaseidNameidCidGet(String databaseid, Integer nameid, String cid) throws ApiException {
    return commonnamesDatabaseidNameidCidGetWithHttpInfo(databaseid, nameid, cid).getData();
      }

  /**
   * the common name dataset
   * An array with one element containing the commonname dataset
   * @param databaseid database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @param cid database unique id of the common name, URL-encoded (required)
   * @return ApiResponse&lt;List&lt;Commonname&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Commonname>> commonnamesDatabaseidNameidCidGetWithHttpInfo(String databaseid, Integer nameid, String cid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling commonnamesDatabaseidNameidCidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling commonnamesDatabaseidNameidCidGet");
    }
    
    // verify the required parameter 'cid' is set
    if (cid == null) {
      throw new ApiException(400, "Missing the required parameter 'cid' when calling commonnamesDatabaseidNameidCidGet");
    }
    
    // create path and map variables
    String localVarPath = "/commonnames/{databaseid}/{nameid}/{cid}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()))
      .replaceAll("\\{" + "cid" + "\\}", apiClient.escapeString(cid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Commonname>> localVarReturnType = new GenericType<List<Commonname>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Query the common name dataset
   * Exact search on all common names
   * @param exactcommonname Exact common name to search (required)
   * @return List&lt;Commonname&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Commonname> commonnamesGet(String exactcommonname) throws ApiException {
    return commonnamesGetWithHttpInfo(exactcommonname).getData();
      }

  /**
   * Query the common name dataset
   * Exact search on all common names
   * @param exactcommonname Exact common name to search (required)
   * @return ApiResponse&lt;List&lt;Commonname&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Commonname>> commonnamesGetWithHttpInfo(String exactcommonname) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'exactcommonname' is set
    if (exactcommonname == null) {
      throw new ApiException(400, "Missing the required parameter 'exactcommonname' when calling commonnamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/commonnames/";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "exactcommonname", exactcommonname));

    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Commonname>> localVarReturnType = new GenericType<List<Commonname>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * All parent names and the hierarchy defining projects
   * An array with links to all parent names in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param projectid database unique id of the project which defined the hierarchy (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Hierarchyfull&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Hierarchyfull> hierarchyDatabaseidProjectidNameidFullGet(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    return hierarchyDatabaseidProjectidNameidFullGetWithHttpInfo(databaseid, projectid, nameid).getData();
      }

  /**
   * All parent names and the hierarchy defining projects
   * An array with links to all parent names in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param projectid database unique id of the project which defined the hierarchy (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Hierarchyfull&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Hierarchyfull>> hierarchyDatabaseidProjectidNameidFullGetWithHttpInfo(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling hierarchyDatabaseidProjectidNameidFullGet");
    }
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling hierarchyDatabaseidProjectidNameidFullGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling hierarchyDatabaseidProjectidNameidFullGet");
    }
    
    // create path and map variables
    String localVarPath = "/hierarchy/{databaseid}/{projectid}/{nameid}/full"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Hierarchyfull>> localVarReturnType = new GenericType<List<Hierarchyfull>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * the parent name and the hierarchy defining project
   * An array with links to the parent name in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param projectid database unique id of the project which defined the hierarchy (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Hierarchy&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Hierarchy> hierarchyDatabaseidProjectidNameidGet(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    return hierarchyDatabaseidProjectidNameidGetWithHttpInfo(databaseid, projectid, nameid).getData();
      }

  /**
   * the parent name and the hierarchy defining project
   * An array with links to the parent name in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param projectid database unique id of the project which defined the hierarchy (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Hierarchy&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Hierarchy>> hierarchyDatabaseidProjectidNameidGetWithHttpInfo(String databaseid, Integer projectid, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling hierarchyDatabaseidProjectidNameidGet");
    }
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling hierarchyDatabaseidProjectidNameidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling hierarchyDatabaseidProjectidNameidGet");
    }
    
    // create path and map variables
    String localVarPath = "/hierarchy/{databaseid}/{projectid}/{nameid}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Hierarchy>> localVarReturnType = new GenericType<List<Hierarchy>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Search of the id of the accepted name
   * An database search for the accepted name for this taxonomic name. The default acceptedname-project is used for the search.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name for which the accepted name is searched (required)
   * @return List&lt;Acceptednameresult&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Acceptednameresult> namesDatabaseNameidAcceptednameidGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidAcceptednameidGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * Search of the id of the accepted name
   * An database search for the accepted name for this taxonomic name. The default acceptedname-project is used for the search.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name for which the accepted name is searched (required)
   * @return ApiResponse&lt;List&lt;Acceptednameresult&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Acceptednameresult>> namesDatabaseNameidAcceptednameidGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidAcceptednameidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidAcceptednameidGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/acceptednameid"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Acceptednameresult>> localVarReturnType = new GenericType<List<Acceptednameresult>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * the corresponding accepted names
   * A list of links to accepted names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Acceptednamelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Acceptednamelink> namesDatabaseNameidAcceptednamesGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidAcceptednamesGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * the corresponding accepted names
   * A list of links to accepted names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Acceptednamelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Acceptednamelink>> namesDatabaseNameidAcceptednamesGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidAcceptednamesGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidAcceptednamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/acceptednames"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Acceptednamelink>> localVarReturnType = new GenericType<List<Acceptednamelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * the corresponding common names
   * get a list of links to corresponding common names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Commonnamelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Commonnamelink> namesDatabaseNameidCommonnamesGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidCommonnamesGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * the corresponding common names
   * get a list of links to corresponding common names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Commonnamelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Commonnamelink>> namesDatabaseNameidCommonnamesGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidCommonnamesGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidCommonnamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/commonnames"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Commonnamelink>> localVarReturnType = new GenericType<List<Commonnamelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * links to external sources
   * Direct references to external sources for the same name. The URIs are the external identifiers.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;ExternalIdentifiers&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ExternalIdentifiers> namesDatabaseNameidExternalidentifiersGet(String database, BigDecimal nameid) throws ApiException {
    return namesDatabaseNameidExternalidentifiersGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * links to external sources
   * Direct references to external sources for the same name. The URIs are the external identifiers.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;ExternalIdentifiers&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ExternalIdentifiers>> namesDatabaseNameidExternalidentifiersGetWithHttpInfo(String database, BigDecimal nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidExternalidentifiersGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidExternalidentifiersGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/externalidentifiers"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<ExternalIdentifiers>> localVarReturnType = new GenericType<List<ExternalIdentifiers>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * specific taxonomic name
   * Get one taxonomic name data set with link to further information
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Taxonname&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Taxonname> namesDatabaseNameidGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * specific taxonomic name
   * Get one taxonomic name data set with link to further information
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Taxonname&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Taxonname>> namesDatabaseNameidGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Taxonname>> localVarReturnType = new GenericType<List<Taxonname>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * the parents of all available hierachies for this taxonomic name
   * A list of links to parent names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Hierarchylink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Hierarchylink> namesDatabaseNameidHierarchiesGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidHierarchiesGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * the parents of all available hierachies for this taxonomic name
   * A list of links to parent names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Hierarchylink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Hierarchylink>> namesDatabaseNameidHierarchiesGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidHierarchiesGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidHierarchiesGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/hierarchies"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Hierarchylink>> localVarReturnType = new GenericType<List<Hierarchylink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * A list of all taxonomic lists in this database which contain this taxonomic name
   * A list of links to taxonomic lists which contain this taxonomic name in this database. Additionally a link to possible analysis categories is given.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Listslink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Listslink> namesDatabaseNameidListsGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidListsGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * A list of all taxonomic lists in this database which contain this taxonomic name
   * A list of links to taxonomic lists which contain this taxonomic name in this database. Additionally a link to possible analysis categories is given.
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Listslink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Listslink>> namesDatabaseNameidListsGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidListsGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidListsGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/lists"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Listslink>> localVarReturnType = new GenericType<List<Listslink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Synonyms
   * A list of synonymic names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return List&lt;Synonymnamelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Synonymnamelink> namesDatabaseNameidSynonymsGet(String database, Integer nameid) throws ApiException {
    return namesDatabaseNameidSynonymsGetWithHttpInfo(database, nameid).getData();
      }

  /**
   * Synonyms
   * A list of synonymic names for this taxonomic name
   * @param database database name (required)
   * @param nameid database unique id of the taxonomic name (required)
   * @return ApiResponse&lt;List&lt;Synonymnamelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Synonymnamelink>> namesDatabaseNameidSynonymsGetWithHttpInfo(String database, Integer nameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'database' is set
    if (database == null) {
      throw new ApiException(400, "Missing the required parameter 'database' when calling namesDatabaseNameidSynonymsGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling namesDatabaseNameidSynonymsGet");
    }
    
    // create path and map variables
    String localVarPath = "/names/{database}/{nameid}/synonyms"
      .replaceAll("\\{" + "database" + "\\}", apiClient.escapeString(database.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Synonymnamelink>> localVarReturnType = new GenericType<List<Synonymnamelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Taxonomic names search
   * &lt;p&gt;Full text search on names&lt;/p&gt;&lt;p&gt; The search covers the fields: &lt;ul&gt;&lt;li&gt;TaxonNameCache&lt;/li&gt;&lt;li&gt;CommonName&lt;/li&gt;&lt;li&gt;CountryCode&lt;/li&gt;&lt;li&gt;DatabaseName&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt; Use ~N to allow N uncertain letters: colo~2 will search everything by exchanging two letters in colo: colo, coco, cora, ...&lt;/p&gt;&lt;p&gt;Additional search possiblities with:&lt;ul&gt;&lt;li&gt;Search for NameID with the parameter nameid.&lt;/li&gt;&lt;li&gt;Search for exact matches in attribute TaxonNameCache with parameter exactname.&lt;/li&gt;&lt;ul&gt;&lt;/p&gt;&lt;p&gt;Parameters should not be used together in one query. One parameter has to be given..&lt;/p&gt;
   * @param name Text to search (optional)
   * @param exactname Exact name to search in field TaxonNameCache (optional)
   * @param nameid NameID to search for in all lists (optional)
   * @param exactnamepartwithoutauthorandyear Search only for the first part of a name without authors and year. Only exact matches are returned. (optional)
   * @return List&lt;Resultlist&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Resultlist> namesGet(String name, String exactname, String nameid, String exactnamepartwithoutauthorandyear) throws ApiException {
    return namesGetWithHttpInfo(name, exactname, nameid, exactnamepartwithoutauthorandyear).getData();
      }

  /**
   * Taxonomic names search
   * &lt;p&gt;Full text search on names&lt;/p&gt;&lt;p&gt; The search covers the fields: &lt;ul&gt;&lt;li&gt;TaxonNameCache&lt;/li&gt;&lt;li&gt;CommonName&lt;/li&gt;&lt;li&gt;CountryCode&lt;/li&gt;&lt;li&gt;DatabaseName&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt; Use ~N to allow N uncertain letters: colo~2 will search everything by exchanging two letters in colo: colo, coco, cora, ...&lt;/p&gt;&lt;p&gt;Additional search possiblities with:&lt;ul&gt;&lt;li&gt;Search for NameID with the parameter nameid.&lt;/li&gt;&lt;li&gt;Search for exact matches in attribute TaxonNameCache with parameter exactname.&lt;/li&gt;&lt;ul&gt;&lt;/p&gt;&lt;p&gt;Parameters should not be used together in one query. One parameter has to be given..&lt;/p&gt;
   * @param name Text to search (optional)
   * @param exactname Exact name to search in field TaxonNameCache (optional)
   * @param nameid NameID to search for in all lists (optional)
   * @param exactnamepartwithoutauthorandyear Search only for the first part of a name without authors and year. Only exact matches are returned. (optional)
   * @return ApiResponse&lt;List&lt;Resultlist&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Resultlist>> namesGetWithHttpInfo(String name, String exactname, String nameid, String exactnamepartwithoutauthorandyear) throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/names";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "exactname", exactname));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "nameid", nameid));
    localVarQueryParams.addAll(apiClient.parameterToPairs("", "exactnamepartwithoutauthorandyear", exactnamepartwithoutauthorandyear));

    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Resultlist>> localVarReturnType = new GenericType<List<Resultlist>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * Synonyms
   * An array with a links to the synonym
   * @param databaseid database name (required)
   * @param projectid database unique id of the project in which this synonym is defined (required)
   * @param nameid database unique id of the name (required)
   * @param synnameid database unique id of the synonym name (required)
   * @return List&lt;Synonymname&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Synonymname> synonymyDatabaseidProjectidNameidSynnameidGet(String databaseid, Integer projectid, Integer nameid, Integer synnameid) throws ApiException {
    return synonymyDatabaseidProjectidNameidSynnameidGetWithHttpInfo(databaseid, projectid, nameid, synnameid).getData();
      }

  /**
   * Synonyms
   * An array with a links to the synonym
   * @param databaseid database name (required)
   * @param projectid database unique id of the project in which this synonym is defined (required)
   * @param nameid database unique id of the name (required)
   * @param synnameid database unique id of the synonym name (required)
   * @return ApiResponse&lt;List&lt;Synonymname&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Synonymname>> synonymyDatabaseidProjectidNameidSynnameidGetWithHttpInfo(String databaseid, Integer projectid, Integer nameid, Integer synnameid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling synonymyDatabaseidProjectidNameidSynnameidGet");
    }
    
    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400, "Missing the required parameter 'projectid' when calling synonymyDatabaseidProjectidNameidSynnameidGet");
    }
    
    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400, "Missing the required parameter 'nameid' when calling synonymyDatabaseidProjectidNameidSynnameidGet");
    }
    
    // verify the required parameter 'synnameid' is set
    if (synnameid == null) {
      throw new ApiException(400, "Missing the required parameter 'synnameid' when calling synonymyDatabaseidProjectidNameidSynnameidGet");
    }
    
    // create path and map variables
    String localVarPath = "/synonymy/{databaseid}/{projectid}/{nameid}/{synnameid}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
      .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()))
      .replaceAll("\\{" + "synnameid" + "\\}", apiClient.escapeString(synnameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Synonymname>> localVarReturnType = new GenericType<List<Synonymname>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
