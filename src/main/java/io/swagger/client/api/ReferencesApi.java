package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import javax.ws.rs.core.GenericType;

import io.swagger.client.model.Acceptednamelinks;
import io.swagger.client.model.Agentlists;
import io.swagger.client.model.AnalysisCategoryLinks;
import io.swagger.client.model.Commonnamelinks;
import io.swagger.client.model.Error;
import io.swagger.client.model.Hierarchylinks;
import io.swagger.client.model.Listslinks;
import io.swagger.client.model.Projectlinks;
import io.swagger.client.model.Reference;
import io.swagger.client.model.Referencelink;
import io.swagger.client.model.Referencerelation;
import io.swagger.client.model.Referencerelationslink;
import io.swagger.client.model.Referinglinks;
import io.swagger.client.model.Synonymnamelinks;
import io.swagger.client.model.Taxonnamelinks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")
public class ReferencesApi {
  private ApiClient apiClient;

  public ReferencesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ReferencesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * a specific literature reference relation
   * An array with the datasets and links to all relations on the literature reference. \\ Mostly the author list of the reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @param role role of the relation, like aut for author. (required)
   * @param sequence sequence number of the reference, like number of the author (required)
   * @return List&lt;Referencerelation&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referencerelation> referencerelationDatabaseidReferenceidRoleSequenceGet(String databaseid, Integer referenceid, String role, Integer sequence) throws ApiException {
    return referencerelationDatabaseidReferenceidRoleSequenceGetWithHttpInfo(databaseid, referenceid, role, sequence).getData();
      }

  /**
   * a specific literature reference relation
   * An array with the datasets and links to all relations on the literature reference. \\ Mostly the author list of the reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @param role role of the relation, like aut for author. (required)
   * @param sequence sequence number of the reference, like number of the author (required)
   * @return ApiResponse&lt;List&lt;Referencerelation&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referencerelation>> referencerelationDatabaseidReferenceidRoleSequenceGetWithHttpInfo(String databaseid, Integer referenceid, String role, Integer sequence) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencerelationDatabaseidReferenceidRoleSequenceGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencerelationDatabaseidReferenceidRoleSequenceGet");
    }
    
    // verify the required parameter 'role' is set
    if (role == null) {
      throw new ApiException(400, "Missing the required parameter 'role' when calling referencerelationDatabaseidReferenceidRoleSequenceGet");
    }
    
    // verify the required parameter 'sequence' is set
    if (sequence == null) {
      throw new ApiException(400, "Missing the required parameter 'sequence' when calling referencerelationDatabaseidReferenceidRoleSequenceGet");
    }
    
    // create path and map variables
    String localVarPath = "/referencerelation/{databaseid}/{referenceid}/{role}/{sequence}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()))
      .replaceAll("\\{" + "role" + "\\}", apiClient.escapeString(role.toString()))
      .replaceAll("\\{" + "sequence" + "\\}", apiClient.escapeString(sequence.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referencerelation>> localVarReturnType = new GenericType<List<Referencerelation>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * links to sub/chapters or items in a series
   * An array with links to possible child references which can be chapters, subchapters or items in a series.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Referencelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referencelink> referencesDatabaseidReferenceidChildsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidChildsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * links to sub/chapters or items in a series
   * An array with links to possible child references which can be chapters, subchapters or items in a series.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Referencelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referencelink>> referencesDatabaseidReferenceidChildsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidChildsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidChildsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/childs"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referencelink>> localVarReturnType = new GenericType<List<Referencelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * a specific literature reference
   * An array with links to the parent name in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Reference&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Reference> referencesDatabaseidReferenceidGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * a specific literature reference
   * An array with links to the parent name in the hierarchy and to the hierarchy defining project
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Reference&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Reference>> referencesDatabaseidReferenceidGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Reference>> localVarReturnType = new GenericType<List<Reference>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to accepted names
   * A list of links to accepted names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Acceptednamelinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Acceptednamelinks> referencesDatabaseidReferenceidReferencingAcceptednamesGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingAcceptednamesGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to accepted names
   * A list of links to accepted names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Acceptednamelinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Acceptednamelinks>> referencesDatabaseidReferenceidReferencingAcceptednamesGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingAcceptednamesGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingAcceptednamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/acceptednames"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Acceptednamelinks>> localVarReturnType = new GenericType<List<Acceptednamelinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to agents
   * A list of links to agent-datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Agentlists&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Agentlists> referencesDatabaseidReferenceidReferencingAgentsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingAgentsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to agents
   * A list of links to agent-datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Agentlists&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Agentlists>> referencesDatabaseidReferenceidReferencingAgentsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingAgentsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingAgentsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/agents"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Agentlists>> localVarReturnType = new GenericType<List<Agentlists>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to analysis categories
   * A list of links to analysiscategorie-datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;AnalysisCategoryLinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<AnalysisCategoryLinks> referencesDatabaseidReferenceidReferencingAnalyisisGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingAnalyisisGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to analysis categories
   * A list of links to analysiscategorie-datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategoryLinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<AnalysisCategoryLinks>> referencesDatabaseidReferenceidReferencingAnalyisisGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingAnalyisisGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingAnalyisisGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/analyisis"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<AnalysisCategoryLinks>> localVarReturnType = new GenericType<List<AnalysisCategoryLinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to common names
   * A list of links to common name datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Commonnamelinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Commonnamelinks> referencesDatabaseidReferenceidReferencingCommonnamesGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingCommonnamesGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to common names
   * A list of links to common name datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Commonnamelinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Commonnamelinks>> referencesDatabaseidReferenceidReferencingCommonnamesGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingCommonnamesGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingCommonnamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/commonnames"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Commonnamelinks>> localVarReturnType = new GenericType<List<Commonnamelinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to datasets
   * A list of links which provide information on datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Referinglinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referinglinks> referencesDatabaseidReferenceidReferencingGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to datasets
   * A list of links which provide information on datasets citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Referinglinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referinglinks>> referencesDatabaseidReferenceidReferencingGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referinglinks>> localVarReturnType = new GenericType<List<Referinglinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to hierarchies
   * A list of links to hierarchies citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Hierarchylinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Hierarchylinks> referencesDatabaseidReferenceidReferencingHierarchiesGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingHierarchiesGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to hierarchies
   * A list of links to hierarchies citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Hierarchylinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Hierarchylinks>> referencesDatabaseidReferenceidReferencingHierarchiesGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingHierarchiesGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingHierarchiesGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/hierarchies"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Hierarchylinks>> localVarReturnType = new GenericType<List<Hierarchylinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to taxonnomic names
   * A list of links to taxonomic names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Taxonnamelinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Taxonnamelinks> referencesDatabaseidReferenceidReferencingNamesGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingNamesGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to taxonnomic names
   * A list of links to taxonomic names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Taxonnamelinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Taxonnamelinks>> referencesDatabaseidReferenceidReferencingNamesGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingNamesGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingNamesGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/names"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Taxonnamelinks>> localVarReturnType = new GenericType<List<Taxonnamelinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to projects
   * A list of links to projects citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Projectlinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Projectlinks> referencesDatabaseidReferenceidReferencingProjectsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingProjectsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to projects
   * A list of links to projects citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Projectlinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Projectlinks>> referencesDatabaseidReferenceidReferencingProjectsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingProjectsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingProjectsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/projects"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Projectlinks>> localVarReturnType = new GenericType<List<Projectlinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to synonyms
   * A list of links to synonyms citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Synonymnamelinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Synonymnamelinks> referencesDatabaseidReferenceidReferencingSynonymsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingSynonymsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to synonyms
   * A list of links to synonyms citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Synonymnamelinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Synonymnamelinks>> referencesDatabaseidReferenceidReferencingSynonymsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingSynonymsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingSynonymsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/synonyms"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Synonymnamelinks>> localVarReturnType = new GenericType<List<Synonymnamelinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * List of links to taxonnomic lists
   * A list of links to taxonomic lists containing names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Listslinks&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Listslinks> referencesDatabaseidReferenceidReferencingTaxonlistsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidReferencingTaxonlistsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * List of links to taxonnomic lists
   * A list of links to taxonomic lists containing names citing this reference.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Listslinks&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Listslinks>> referencesDatabaseidReferenceidReferencingTaxonlistsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidReferencingTaxonlistsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidReferencingTaxonlistsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/referencing/taxonlists"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Listslinks>> localVarReturnType = new GenericType<List<Listslinks>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * a specific literature reference relation
   * An array with the datasets and links to all relations on the literature reference. \\ Mostly the author list of the reference. \\ The links are only for referencing an specific relation.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return List&lt;Referencerelationslink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referencerelationslink> referencesDatabaseidReferenceidRelationsGet(String databaseid, Integer referenceid) throws ApiException {
    return referencesDatabaseidReferenceidRelationsGetWithHttpInfo(databaseid, referenceid).getData();
      }

  /**
   * a specific literature reference relation
   * An array with the datasets and links to all relations on the literature reference. \\ Mostly the author list of the reference. \\ The links are only for referencing an specific relation.
   * @param databaseid database name (required)
   * @param referenceid database unique id of the reference (required)
   * @return ApiResponse&lt;List&lt;Referencerelationslink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referencerelationslink>> referencesDatabaseidReferenceidRelationsGetWithHttpInfo(String databaseid, Integer referenceid) throws ApiException {
    Object localVarPostBody = null;
    
    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400, "Missing the required parameter 'databaseid' when calling referencesDatabaseidReferenceidRelationsGet");
    }
    
    // verify the required parameter 'referenceid' is set
    if (referenceid == null) {
      throw new ApiException(400, "Missing the required parameter 'referenceid' when calling referencesDatabaseidReferenceidRelationsGet");
    }
    
    // create path and map variables
    String localVarPath = "/references/{databaseid}/{referenceid}/relations"
      .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
      .replaceAll("\\{" + "referenceid" + "\\}", apiClient.escapeString(referenceid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referencerelationslink>> localVarReturnType = new GenericType<List<Referencerelationslink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * literature references
   * An array with links to the literature references
   * @return List&lt;Referencelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<Referencelink> referencesGet() throws ApiException {
    return referencesGetWithHttpInfo().getData();
      }

  /**
   * literature references
   * An array with links to the literature references
   * @return ApiResponse&lt;List&lt;Referencelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<Referencelink>> referencesGetWithHttpInfo() throws ApiException {
    Object localVarPostBody = null;
    
    // create path and map variables
    String localVarPath = "/references";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    
    
    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<List<Referencelink>> localVarReturnType = new GenericType<List<Referencelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
}
