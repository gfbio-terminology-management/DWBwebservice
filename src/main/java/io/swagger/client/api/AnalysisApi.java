package io.swagger.client.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.GenericType;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.model.AnalysisCategoryValues;
import io.swagger.client.model.AnalysisValues;
import io.swagger.client.model.LinksAnalysisCategories;
import io.swagger.client.model.ListAnalysisCategoriesfornamelink;
import io.swagger.client.model.ListAnalysisCategorieslink;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen",
    date = "2020-11-10T08:47:07.898Z")
public class AnalysisApi {
  private ApiClient apiClient;

  public AnalysisApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AnalysisApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * link to a list with analysis values for the given name A list with the assigned analysis value
   * in the given analysis category for the given NameID
   * 
   * @param databaseid database name (required)
   * @param projectid ProejctID of the list (required)
   * @param nameid NameID of the list item (the txonomic name) (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return List&lt;AnalysisValues&gt;
   * @throws ApiException if fails to make API call
   */
  public List<AnalysisValues> analysisDatabaseidProjectidNameidAnalysisidGet(String databaseid,
      Integer projectid, Integer nameid, Integer analysisid) throws ApiException {
    return analysisDatabaseidProjectidNameidAnalysisidGetWithHttpInfo(databaseid, projectid, nameid,
        analysisid).getData();
  }

  /**
   * link to a list with analysis values for the given name A list with the assigned analysis value
   * in the given analysis category for the given NameID
   * 
   * @param databaseid database name (required)
   * @param projectid ProejctID of the list (required)
   * @param nameid NameID of the list item (the txonomic name) (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return ApiResponse&lt;List&lt;AnalysisValues&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<AnalysisValues>> analysisDatabaseidProjectidNameidAnalysisidGetWithHttpInfo(
      String databaseid, Integer projectid, Integer nameid, Integer analysisid)
      throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'databaseid' when calling analysisDatabaseidProjectidNameidAnalysisidGet");
    }

    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'projectid' when calling analysisDatabaseidProjectidNameidAnalysisidGet");
    }

    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'nameid' when calling analysisDatabaseidProjectidNameidAnalysisidGet");
    }

    // verify the required parameter 'analysisid' is set
    if (analysisid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analysisid' when calling analysisDatabaseidProjectidNameidAnalysisidGet");
    }

    // create path and map variables
    String localVarPath = "/analysis/{databaseid}/{projectid}/{nameid}/{analysisid}"
        .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
        .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
        .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()))
        .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<AnalysisValues>> localVarReturnType =
        new GenericType<List<AnalysisValues>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * link to a list with analysis categories for the given name A list with the assigned analysis
   * category for the given NameID
   * 
   * @param databaseid database name (required)
   * @param projectid ProejctID of the list (required)
   * @param nameid NameID of the list item (the txonomic name) (required)
   * @return List&lt;ListAnalysisCategoriesfornamelink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ListAnalysisCategoriesfornamelink> analysisDatabaseidProjectidNameidGet(
      String databaseid, Integer projectid, Integer nameid) throws ApiException {
    return analysisDatabaseidProjectidNameidGetWithHttpInfo(databaseid, projectid, nameid)
        .getData();
  }

  /**
   * link to a list with analysis categories for the given name A list with the assigned analysis
   * category for the given NameID
   * 
   * @param databaseid database name (required)
   * @param projectid ProejctID of the list (required)
   * @param nameid NameID of the list item (the txonomic name) (required)
   * @return ApiResponse&lt;List&lt;ListAnalysisCategoriesfornamelink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ListAnalysisCategoriesfornamelink>> analysisDatabaseidProjectidNameidGetWithHttpInfo(
      String databaseid, Integer projectid, Integer nameid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'databaseid' when calling analysisDatabaseidProjectidNameidGet");
    }

    // verify the required parameter 'projectid' is set
    if (projectid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'projectid' when calling analysisDatabaseidProjectidNameidGet");
    }

    // verify the required parameter 'nameid' is set
    if (nameid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'nameid' when calling analysisDatabaseidProjectidNameidGet");
    }

    // create path and map variables
    String localVarPath = "/analysis/{databaseid}/{projectid}/{nameid}"
        .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
        .replaceAll("\\{" + "projectid" + "\\}", apiClient.escapeString(projectid.toString()))
        .replaceAll("\\{" + "nameid" + "\\}", apiClient.escapeString(nameid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<ListAnalysisCategoriesfornamelink>> localVarReturnType =
        new GenericType<List<ListAnalysisCategoriesfornamelink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * List of all sub analysis category definitions Generates a list of sub (child) analysis
   * categories for the given analysis ID
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return List&lt;AnalysisCategories&gt;
   * @throws ApiException if fails to make API call
   */

  // public List<AnalysisCategories> analysiscategoriesDatabaseidAnalysisidChildsGet(String
  // databaseid, Integer analysisid) throws ApiException {
  // return analysiscategoriesDatabaseidAnalysisidChildsGetWithHttpInfo(databaseid,
  // analysisid).getData();
  // }

  /**
   * List of all sub analysis category definitions Generates a list of sub (child) analysis
   * categories for the given analysis ID
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategories&gt;&gt;
   * @throws ApiException if fails to make API call
   */

  // public ApiResponse<List<AnalysisCategories>>
  // analysiscategoriesDatabaseidAnalysisidChildsGetWithHttpInfo(String databaseid, Integer
  // analysisid) throws ApiException {
  // Object localVarPostBody = null;
  //
  // // verify the required parameter 'databaseid' is set
  // if (databaseid == null) {
  // throw new ApiException(400, "Missing the required parameter 'databaseid' when calling
  // analysiscategoriesDatabaseidAnalysisidChildsGet");
  // }
  //
  // // verify the required parameter 'analysisid' is set
  // if (analysisid == null) {
  // throw new ApiException(400, "Missing the required parameter 'analysisid' when calling
  // analysiscategoriesDatabaseidAnalysisidChildsGet");
  // }
  //
  // // create path and map variables
  // String localVarPath = "/analysiscategories/{databaseid}/{analysisid}/childs"
  // .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
  // .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));
  //
  // // query params
  // List<Pair> localVarQueryParams = new ArrayList<Pair>();
  // Map<String, String> localVarHeaderParams = new HashMap<String, String>();
  // Map<String, Object> localVarFormParams = new HashMap<String, Object>();
  //
  //
  //
  //
  // final String[] localVarAccepts = {
  // "application/json"
  // };
  // final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
  //
  // final String[] localVarContentTypes = {
  //
  // };
  // final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
  //
  // String[] localVarAuthNames = new String[] { };
  //
  // GenericType<List<AnalysisCategories>> localVarReturnType = new
  // GenericType<List<AnalysisCategories>>() {};
  // return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
  // localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
  // localVarAuthNames, localVarReturnType);
  // }
  /**
   * a specific analysis category definition Definiton the analysis category with the given analysis
   * ID
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return List&lt;AnalysisCategories&gt;
   * @throws ApiException if fails to make API call
   */
  // public List<AnalysisCategories> analysiscategoriesDatabaseidAnalysisidGet(String databaseid,
  // Integer analysisid) throws ApiException {
  // return analysiscategoriesDatabaseidAnalysisidGetWithHttpInfo(databaseid, analysisid).getData();
  // }

  /**
   * a specific analysis category definition Definiton the analysis category with the given analysis
   * ID
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategories&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  // public ApiResponse<List<AnalysisCategories>>
  // analysiscategoriesDatabaseidAnalysisidGetWithHttpInfo(
  // String databaseid, Integer analysisid) throws ApiException {
  // Object localVarPostBody = null;
  //
  // // verify the required parameter 'databaseid' is set
  // if (databaseid == null) {
  // throw new ApiException(400,
  // "Missing the required parameter 'databaseid' when calling
  // analysiscategoriesDatabaseidAnalysisidGet");
  // }
  //
  // // verify the required parameter 'analysisid' is set
  // if (analysisid == null) {
  // throw new ApiException(400,
  // "Missing the required parameter 'analysisid' when calling
  // analysiscategoriesDatabaseidAnalysisidGet");
  // }
  //
  // // create path and map variables
  // String localVarPath = "/analysiscategories/{databaseid}/{analysisid}"
  // .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
  // .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));
  //
  // // query params
  // List<Pair> localVarQueryParams = new ArrayList<Pair>();
  // Map<String, String> localVarHeaderParams = new HashMap<String, String>();
  // Map<String, Object> localVarFormParams = new HashMap<String, Object>();
  //
  //
  //
  // final String[] localVarAccepts = {"application/json"};
  // final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
  //
  // final String[] localVarContentTypes = {
  //
  // };
  // final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
  //
  // String[] localVarAuthNames = new String[] {};
  //
  // GenericType<List<AnalysisCategories>> localVarReturnType =
  // new GenericType<List<AnalysisCategories>>() {};
  // return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
  // localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
  // localVarAuthNames, localVarReturnType);
  // }

  /**
   * List of links to projects Provides links to projects/lists which have tis analysis catetgory
   * assignd to some of their items
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return List&lt;ListAnalysisCategorieslink&gt;
   * @throws ApiException if fails to make API call
   */
  public List<ListAnalysisCategorieslink> analysiscategoriesDatabaseidAnalysisidProjectsGet(
      String databaseid, Integer analysisid) throws ApiException {
    return analysiscategoriesDatabaseidAnalysisidProjectsGetWithHttpInfo(databaseid, analysisid)
        .getData();
  }

  /**
   * List of links to projects Provides links to projects/lists which have tis analysis catetgory
   * assignd to some of their items
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return ApiResponse&lt;List&lt;ListAnalysisCategorieslink&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<ListAnalysisCategorieslink>> analysiscategoriesDatabaseidAnalysisidProjectsGetWithHttpInfo(
      String databaseid, Integer analysisid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'databaseid' when calling analysiscategoriesDatabaseidAnalysisidProjectsGet");
    }

    // verify the required parameter 'analysisid' is set
    if (analysisid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analysisid' when calling analysiscategoriesDatabaseidAnalysisidProjectsGet");
    }

    // create path and map variables
    String localVarPath = "/analysiscategories/{databaseid}/{analysisid}/projects"
        .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
        .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<ListAnalysisCategorieslink>> localVarReturnType =
        new GenericType<List<ListAnalysisCategorieslink>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * link to a list with predefined analysis values Link to a list with all predefined (categorial)
   * analysis values
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return List&lt;AnalysisCategoryValues&gt;
   * @throws ApiException if fails to make API call
   */
  public List<AnalysisCategoryValues> analysiscategoriesDatabaseidAnalysisidValuedefinitionsGet(
      String databaseid, Integer analysisid) throws ApiException {
    return analysiscategoriesDatabaseidAnalysisidValuedefinitionsGetWithHttpInfo(databaseid,
        analysisid).getData();
  }

  /**
   * link to a list with predefined analysis values Link to a list with all predefined (categorial)
   * analysis values
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategoryValues&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<AnalysisCategoryValues>> analysiscategoriesDatabaseidAnalysisidValuedefinitionsGetWithHttpInfo(
      String databaseid, Integer analysisid) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'databaseid' when calling analysiscategoriesDatabaseidAnalysisidValuedefinitionsGet");
    }

    // verify the required parameter 'analysisid' is set
    if (analysisid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analysisid' when calling analysiscategoriesDatabaseidAnalysisidValuedefinitionsGet");
    }

    // create path and map variables
    String localVarPath = "/analysiscategories/{databaseid}/{analysisid}/valuedefinitions"
        .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
        .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<AnalysisCategoryValues>> localVarReturnType =
        new GenericType<List<AnalysisCategoryValues>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * all used analysis categories in all lists An array with the analysis category definitions and
   * links to the analysis category, a parent category if given and optional defined categorial
   * analysis values
   * 
   * @return List&lt;LinksAnalysisCategories&gt;
   * @throws ApiException if fails to make API call
   */
  public List<LinksAnalysisCategories> analysiscategoriesGet() throws ApiException {
    return analysiscategoriesGetWithHttpInfo().getData();
  }

  /**
   * all used analysis categories in all lists An array with the analysis category definitions and
   * links to the analysis category, a parent category if given and optional defined categorial
   * analysis values
   * 
   * @return ApiResponse&lt;List&lt;LinksAnalysisCategories&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<LinksAnalysisCategories>> analysiscategoriesGetWithHttpInfo()
      throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/analysiscategories/";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<LinksAnalysisCategories>> localVarReturnType =
        new GenericType<List<LinksAnalysisCategories>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }

  /**
   * predefined analysis value A predefined (categorial) analysis values in this analysis category
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @param analyisvalue analysis value url-encoded (required)
   * @return List&lt;AnalysisCategoryValues&gt;
   * @throws ApiException if fails to make API call
   */
  public List<AnalysisCategoryValues> analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGet(
      String databaseid, Integer analysisid, String analyisvalue) throws ApiException {
    return analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGetWithHttpInfo(databaseid,
        analysisid, analyisvalue).getData();
  }

  /**
   * predefined analysis value A predefined (categorial) analysis values in this analysis category
   * 
   * @param databaseid database name (required)
   * @param analysisid database unique id of the analysis category (required)
   * @param analyisvalue analysis value url-encoded (required)
   * @return ApiResponse&lt;List&lt;AnalysisCategoryValues&gt;&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<List<AnalysisCategoryValues>> analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGetWithHttpInfo(
      String databaseid, Integer analysisid, String analyisvalue) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'databaseid' is set
    if (databaseid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'databaseid' when calling analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGet");
    }

    // verify the required parameter 'analysisid' is set
    if (analysisid == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analysisid' when calling analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGet");
    }

    // verify the required parameter 'analyisvalue' is set
    if (analyisvalue == null) {
      throw new ApiException(400,
          "Missing the required parameter 'analyisvalue' when calling analysiscategorievaluesDatabaseidAnalysisidAnalyisvalueGet");
    }

    // create path and map variables
    String localVarPath = "/analysiscategorievalues/{databaseid}/{analysisid}/{analyisvalue}"
        .replaceAll("\\{" + "databaseid" + "\\}", apiClient.escapeString(databaseid.toString()))
        .replaceAll("\\{" + "analysisid" + "\\}", apiClient.escapeString(analysisid.toString()))
        .replaceAll("\\{" + "analyisvalue" + "\\}",
            apiClient.escapeString(analyisvalue.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();



    final String[] localVarAccepts = {"application/json"};
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {};

    GenericType<List<AnalysisCategoryValues>> localVarReturnType =
        new GenericType<List<AnalysisCategoryValues>>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody,
        localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType,
        localVarAuthNames, localVarReturnType);
  }
}
