/*
 * Regionalised and Domain-specific Taxon Lists
 * <p>The DWB REST Webservice for Taxon Lists is part of a Diversity Workbench (DWB) services network. It is delivering basic information on taxon names in use, synonyms, classification and German vernacular names of a number of groups of animals, fungi and plants.</p><p>The current focus is on domain-specific lists (checklists, taxon reference lists, red lists) from Germany under active curation by experts on taxonomy or floristics and faunistics. Each regionalised and domain-specific taxon list has its own history and objectives, is managed completely separately and has its own hierarchical classification. The DiversityTaxonNames (DTN) data resources accessed by the REST API may include additional taxon-related data useful, e. g., for regional nature conservation agencies and environmental projects.</p></br><p>For more information please check <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\">How to use the DWB REST Webservice for Taxon Lists</a> and <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">Overview on Published Lists</a>. For content details see <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.</p><p><p>Der DWB REST Webservice for Taxon Lists ist Teil eines Netzwerkes aus Diversity Workbench (DWB) Diensten. Er liefert Basisinformation zu anerkannten Taxonnamen, Synonymen, Klassifikationen und deutsche Volksnamen f&uuml;r eine Anzahl von Tier-, Pilz- und Pflanzengruppen. </p> <p>Der Fokus liegt dabei zur Zeit auf dom&auml;n-spezifischen Listen (Checklisten, Taxonreferenzlisten und Rote Listen) aus Deutschland, die von taxonomisch oder floristisch bzw. faunistisch arbeitenden Experten aktiv kuratiert werden. Jede regionalisierte und dom&auml;n-spezifische Taxonliste hat ihre eigene Geschichte und Zielvorstellungen, wird separat gemanagt und hat eine eigenst&auml;ndige hierarchische Klassifikation. Die &uuml;ber die REST API zug&auml;nglichen DiversityTaxonNames (DTN) Datenquellen k&ouml;nnen weitere taxon-bezogene Daten enthalten, die f&uuml;r regionale Naturschutzeinrichtungen und Umweltprojekte von Bedeutung sein k&ouml;nnen.</p><br><p>Hinweise zum Einsatz der REST API finden sich unter <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\" title=\"How to use the DWB REST Webservice for Taxon Lists\">How to use the DWB REST Webservice for Taxon Lists</a>. Die derzeit ver&ouml;ffentlichten Listen werden in einer <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">&Uuml;bersicht</a> angezeigt. Weitere Information zum Inhalt stehen unter <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.<br></p><h1>REST API Documentation</h1>
 *
 * OpenAPI spec version: 0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Link;
import java.util.ArrayList;
import java.util.List;

/**
 * Acceptedname
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")



public class Acceptedname {
  @JsonProperty("ConceptNotes")
  private String conceptNotes = null;

  @JsonProperty("ConceptSuffix")
  private String conceptSuffix = null;

  @JsonProperty("IgnoreButKeepForReference")
  private Integer ignoreButKeepForReference = null;

  @JsonProperty("NameID")
  private Integer nameID = null;

  @JsonProperty("ProjectID")
  private Integer projectID = null;

  @JsonProperty("RefDetail")
  private String refDetail = null;

  @JsonProperty("RefText")
  private String refText = null;

  @JsonProperty("RefURI")
  private String refURI = null;

  @JsonProperty("TypistsNotes")
  private String typistsNotes = null;

  @JsonProperty("links")
  private List<Link> links = null;

  public Acceptedname conceptNotes(String conceptNotes) {
    this.conceptNotes = conceptNotes;
    return this;
  }

   /**
   * Get conceptNotes
   * @return conceptNotes
  **/
  @ApiModelProperty(value = "")
  public String getConceptNotes() {
    return conceptNotes;
  }

  public void setConceptNotes(String conceptNotes) {
    this.conceptNotes = conceptNotes;
  }

  public Acceptedname conceptSuffix(String conceptSuffix) {
    this.conceptSuffix = conceptSuffix;
    return this;
  }

   /**
   * Get conceptSuffix
   * @return conceptSuffix
  **/
  @ApiModelProperty(value = "")
  public String getConceptSuffix() {
    return conceptSuffix;
  }

  public void setConceptSuffix(String conceptSuffix) {
    this.conceptSuffix = conceptSuffix;
  }

  public Acceptedname ignoreButKeepForReference(Integer ignoreButKeepForReference) {
    this.ignoreButKeepForReference = ignoreButKeepForReference;
    return this;
  }

   /**
   * Get ignoreButKeepForReference
   * @return ignoreButKeepForReference
  **/
  @ApiModelProperty(value = "")
  public Integer getIgnoreButKeepForReference() {
    return ignoreButKeepForReference;
  }

  public void setIgnoreButKeepForReference(Integer ignoreButKeepForReference) {
    this.ignoreButKeepForReference = ignoreButKeepForReference;
  }

  public Acceptedname nameID(Integer nameID) {
    this.nameID = nameID;
    return this;
  }

   /**
   * Get nameID
   * @return nameID
  **/
  @ApiModelProperty(value = "")
  public Integer getNameID() {
    return nameID;
  }

  public void setNameID(Integer nameID) {
    this.nameID = nameID;
  }

  public Acceptedname projectID(Integer projectID) {
    this.projectID = projectID;
    return this;
  }

   /**
   * Get projectID
   * @return projectID
  **/
  @ApiModelProperty(value = "")
  public Integer getProjectID() {
    return projectID;
  }

  public void setProjectID(Integer projectID) {
    this.projectID = projectID;
  }

  public Acceptedname refDetail(String refDetail) {
    this.refDetail = refDetail;
    return this;
  }

   /**
   * Get refDetail
   * @return refDetail
  **/
  @ApiModelProperty(value = "")
  public String getRefDetail() {
    return refDetail;
  }

  public void setRefDetail(String refDetail) {
    this.refDetail = refDetail;
  }

  public Acceptedname refText(String refText) {
    this.refText = refText;
    return this;
  }

   /**
   * Get refText
   * @return refText
  **/
  @ApiModelProperty(value = "")
  public String getRefText() {
    return refText;
  }

  public void setRefText(String refText) {
    this.refText = refText;
  }

  public Acceptedname refURI(String refURI) {
    this.refURI = refURI;
    return this;
  }

   /**
   * Get refURI
   * @return refURI
  **/
  @ApiModelProperty(value = "")
  public String getRefURI() {
    return refURI;
  }

  public void setRefURI(String refURI) {
    this.refURI = refURI;
  }

  public Acceptedname typistsNotes(String typistsNotes) {
    this.typistsNotes = typistsNotes;
    return this;
  }

   /**
   * Get typistsNotes
   * @return typistsNotes
  **/
  @ApiModelProperty(value = "")
  public String getTypistsNotes() {
    return typistsNotes;
  }

  public void setTypistsNotes(String typistsNotes) {
    this.typistsNotes = typistsNotes;
  }

  public Acceptedname links(List<Link> links) {
    this.links = links;
    return this;
  }

  public Acceptedname addLinksItem(Link linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Project link to the project description in which this name is defined as an accepted name
   * @return links
  **/
  @ApiModelProperty(value = "Project link to the project description in which this name is defined as an accepted name")
  public List<Link> getLinks() {
    return links;
  }

  public void setLinks(List<Link> links) {
    this.links = links;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Acceptedname acceptedname = (Acceptedname) o;
    return Objects.equals(this.conceptNotes, acceptedname.conceptNotes) &&
        Objects.equals(this.conceptSuffix, acceptedname.conceptSuffix) &&
        Objects.equals(this.ignoreButKeepForReference, acceptedname.ignoreButKeepForReference) &&
        Objects.equals(this.nameID, acceptedname.nameID) &&
        Objects.equals(this.projectID, acceptedname.projectID) &&
        Objects.equals(this.refDetail, acceptedname.refDetail) &&
        Objects.equals(this.refText, acceptedname.refText) &&
        Objects.equals(this.refURI, acceptedname.refURI) &&
        Objects.equals(this.typistsNotes, acceptedname.typistsNotes) &&
        Objects.equals(this.links, acceptedname.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(conceptNotes, conceptSuffix, ignoreButKeepForReference, nameID, projectID, refDetail, refText, refURI, typistsNotes, links);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Acceptedname {\n");
    
    sb.append("    conceptNotes: ").append(toIndentedString(conceptNotes)).append("\n");
    sb.append("    conceptSuffix: ").append(toIndentedString(conceptSuffix)).append("\n");
    sb.append("    ignoreButKeepForReference: ").append(toIndentedString(ignoreButKeepForReference)).append("\n");
    sb.append("    nameID: ").append(toIndentedString(nameID)).append("\n");
    sb.append("    projectID: ").append(toIndentedString(projectID)).append("\n");
    sb.append("    refDetail: ").append(toIndentedString(refDetail)).append("\n");
    sb.append("    refText: ").append(toIndentedString(refText)).append("\n");
    sb.append("    refURI: ").append(toIndentedString(refURI)).append("\n");
    sb.append("    typistsNotes: ").append(toIndentedString(typistsNotes)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

