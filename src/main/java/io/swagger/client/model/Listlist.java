/*
 * Regionalised and Domain-specific Taxon Lists
 * <p>The DWB REST Webservice for Taxon Lists is part of a Diversity Workbench (DWB) services network. It is delivering basic information on taxon names in use, synonyms, classification and German vernacular names of a number of groups of animals, fungi and plants.</p><p>The current focus is on domain-specific lists (checklists, taxon reference lists, red lists) from Germany under active curation by experts on taxonomy or floristics and faunistics. Each regionalised and domain-specific taxon list has its own history and objectives, is managed completely separately and has its own hierarchical classification. The DiversityTaxonNames (DTN) data resources accessed by the REST API may include additional taxon-related data useful, e. g., for regional nature conservation agencies and environmental projects.</p></br><p>For more information please check <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\">How to use the DWB REST Webservice for Taxon Lists</a> and <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">Overview on Published Lists</a>. For content details see <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.</p><p><p>Der DWB REST Webservice for Taxon Lists ist Teil eines Netzwerkes aus Diversity Workbench (DWB) Diensten. Er liefert Basisinformation zu anerkannten Taxonnamen, Synonymen, Klassifikationen und deutsche Volksnamen f&uuml;r eine Anzahl von Tier-, Pilz- und Pflanzengruppen. </p> <p>Der Fokus liegt dabei zur Zeit auf dom&auml;n-spezifischen Listen (Checklisten, Taxonreferenzlisten und Rote Listen) aus Deutschland, die von taxonomisch oder floristisch bzw. faunistisch arbeitenden Experten aktiv kuratiert werden. Jede regionalisierte und dom&auml;n-spezifische Taxonliste hat ihre eigene Geschichte und Zielvorstellungen, wird separat gemanagt und hat eine eigenst&auml;ndige hierarchische Klassifikation. Die &uuml;ber die REST API zug&auml;nglichen DiversityTaxonNames (DTN) Datenquellen k&ouml;nnen weitere taxon-bezogene Daten enthalten, die f&uuml;r regionale Naturschutzeinrichtungen und Umweltprojekte von Bedeutung sein k&ouml;nnen.</p><br><p>Hinweise zum Einsatz der REST API finden sich unter <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\" title=\"How to use the DWB REST Webservice for Taxon Lists\">How to use the DWB REST Webservice for Taxon Lists</a>. Die derzeit ver&ouml;ffentlichten Listen werden in einer <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">&Uuml;bersicht</a> angezeigt. Weitere Information zum Inhalt stehen unter <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.<br></p><h1>REST API Documentation</h1>
 *
 * OpenAPI spec version: 0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Link;
import java.util.ArrayList;
import java.util.List;

/**
 * Listlist
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")



public class Listlist {
  @JsonProperty("DatabaseName")
  private String databaseName = null;

  @JsonProperty("projectid")
  private Integer projectid = null;

  @JsonProperty("projecturi")
  private String projecturi = null;

  @JsonProperty("Projekt")
  private String projekt = null;

  @JsonProperty("links")
  private List<Link> links = null;

  public Listlist databaseName(String databaseName) {
    this.databaseName = databaseName;
    return this;
  }

   /**
   * Get databaseName
   * @return databaseName
  **/
  @ApiModelProperty(value = "")
  public String getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public Listlist projectid(Integer projectid) {
    this.projectid = projectid;
    return this;
  }

   /**
   * Get projectid
   * @return projectid
  **/
  @ApiModelProperty(value = "")
  public Integer getProjectid() {
    return projectid;
  }

  public void setProjectid(Integer projectid) {
    this.projectid = projectid;
  }

  public Listlist projecturi(String projecturi) {
    this.projecturi = projecturi;
    return this;
  }

   /**
   * Get projecturi
   * @return projecturi
  **/
  @ApiModelProperty(value = "")
  public String getProjecturi() {
    return projecturi;
  }

  public void setProjecturi(String projecturi) {
    this.projecturi = projecturi;
  }

  public Listlist projekt(String projekt) {
    this.projekt = projekt;
    return this;
  }

   /**
   * Internal short name of the list/project
   * @return projekt
  **/
  @ApiModelProperty(value = "Internal short name of the list/project")
  public String getProjekt() {
    return projekt;
  }

  public void setProjekt(String projekt) {
    this.projekt = projekt;
  }

  public Listlist links(List<Link> links) {
    this.links = links;
    return this;
  }

  public Listlist addLinksItem(Link linksItem) {
    if (this.links == null) {
      this.links = new ArrayList<>();
    }
    this.links.add(linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @ApiModelProperty(value = "")
  public List<Link> getLinks() {
    return links;
  }

  public void setLinks(List<Link> links) {
    this.links = links;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Listlist listlist = (Listlist) o;
    return Objects.equals(this.databaseName, listlist.databaseName) &&
        Objects.equals(this.projectid, listlist.projectid) &&
        Objects.equals(this.projecturi, listlist.projecturi) &&
        Objects.equals(this.projekt, listlist.projekt) &&
        Objects.equals(this.links, listlist.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(databaseName, projectid, projecturi, projekt, links);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Listlist {\n");
    
    sb.append("    databaseName: ").append(toIndentedString(databaseName)).append("\n");
    sb.append("    projectid: ").append(toIndentedString(projectid)).append("\n");
    sb.append("    projecturi: ").append(toIndentedString(projecturi)).append("\n");
    sb.append("    projekt: ").append(toIndentedString(projekt)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

