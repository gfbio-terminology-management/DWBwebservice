/*
 * Regionalised and Domain-specific Taxon Lists
 * <p>The DWB REST Webservice for Taxon Lists is part of a Diversity Workbench (DWB) services network. It is delivering basic information on taxon names in use, synonyms, classification and German vernacular names of a number of groups of animals, fungi and plants.</p><p>The current focus is on domain-specific lists (checklists, taxon reference lists, red lists) from Germany under active curation by experts on taxonomy or floristics and faunistics. Each regionalised and domain-specific taxon list has its own history and objectives, is managed completely separately and has its own hierarchical classification. The DiversityTaxonNames (DTN) data resources accessed by the REST API may include additional taxon-related data useful, e. g., for regional nature conservation agencies and environmental projects.</p></br><p>For more information please check <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\">How to use the DWB REST Webservice for Taxon Lists</a> and <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">Overview on Published Lists</a>. For content details see <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.</p><p><p>Der DWB REST Webservice for Taxon Lists ist Teil eines Netzwerkes aus Diversity Workbench (DWB) Diensten. Er liefert Basisinformation zu anerkannten Taxonnamen, Synonymen, Klassifikationen und deutsche Volksnamen f&uuml;r eine Anzahl von Tier-, Pilz- und Pflanzengruppen. </p> <p>Der Fokus liegt dabei zur Zeit auf dom&auml;n-spezifischen Listen (Checklisten, Taxonreferenzlisten und Rote Listen) aus Deutschland, die von taxonomisch oder floristisch bzw. faunistisch arbeitenden Experten aktiv kuratiert werden. Jede regionalisierte und dom&auml;n-spezifische Taxonliste hat ihre eigene Geschichte und Zielvorstellungen, wird separat gemanagt und hat eine eigenst&auml;ndige hierarchische Klassifikation. Die &uuml;ber die REST API zug&auml;nglichen DiversityTaxonNames (DTN) Datenquellen k&ouml;nnen weitere taxon-bezogene Daten enthalten, die f&uuml;r regionale Naturschutzeinrichtungen und Umweltprojekte von Bedeutung sein k&ouml;nnen.</p><br><p>Hinweise zum Einsatz der REST API finden sich unter <a href=\"http://www.diversitymobile.net/wiki/How_to_use_the_DWB_REST_Webservice_for_Taxon_Lists\" title=\"How to use the DWB REST Webservice for Taxon Lists\">How to use the DWB REST Webservice for Taxon Lists</a>. Die derzeit ver&ouml;ffentlichten Listen werden in einer <a href=\"http://services.snsb.info/DTNtaxonlists/rest/v0.1/\">&Uuml;bersicht</a> angezeigt. Weitere Information zum Inhalt stehen unter <a href=\"http://www.diversitymobile.net/wiki/DTN_Taxon_Lists_Services\">DTN Taxon Lists Services</a>.<br></p><h1>REST API Documentation</h1>
 *
 * OpenAPI spec version: 0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ExternalNameSource
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-11-10T08:47:07.898Z")



public class ExternalNameSource {
  @JsonProperty("DatabaseName")
  private String databaseName = null;

  @JsonProperty("ExternalDatabaseID")
  private Integer externalDatabaseID = null;

  @JsonProperty("ExternalDatabaseName")
  private String externalDatabaseName = null;

  @JsonProperty("ExternalDatabaseVersion")
  private String externalDatabaseVersion = null;

  @JsonProperty("Rights")
  private String rights = null;

  @JsonProperty("ExternalDatabaseAuthors")
  private String externalDatabaseAuthors = null;

  @JsonProperty("ExternalDatabaseURI")
  private String externalDatabaseURI = null;

  public ExternalNameSource databaseName(String databaseName) {
    this.databaseName = databaseName;
    return this;
  }

   /**
   * DTN database name
   * @return databaseName
  **/
  @ApiModelProperty(value = "DTN database name")
  public String getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public ExternalNameSource externalDatabaseID(Integer externalDatabaseID) {
    this.externalDatabaseID = externalDatabaseID;
    return this;
  }

   /**
   * Database wide unique id of the external source
   * @return externalDatabaseID
  **/
  @ApiModelProperty(value = "Database wide unique id of the external source")
  public Integer getExternalDatabaseID() {
    return externalDatabaseID;
  }

  public void setExternalDatabaseID(Integer externalDatabaseID) {
    this.externalDatabaseID = externalDatabaseID;
  }

  public ExternalNameSource externalDatabaseName(String externalDatabaseName) {
    this.externalDatabaseName = externalDatabaseName;
    return this;
  }

   /**
   * Title of the external source
   * @return externalDatabaseName
  **/
  @ApiModelProperty(value = "Title of the external source")
  public String getExternalDatabaseName() {
    return externalDatabaseName;
  }

  public void setExternalDatabaseName(String externalDatabaseName) {
    this.externalDatabaseName = externalDatabaseName;
  }

  public ExternalNameSource externalDatabaseVersion(String externalDatabaseVersion) {
    this.externalDatabaseVersion = externalDatabaseVersion;
    return this;
  }

   /**
   * Used version of the external source/database
   * @return externalDatabaseVersion
  **/
  @ApiModelProperty(value = "Used version of the external source/database")
  public String getExternalDatabaseVersion() {
    return externalDatabaseVersion;
  }

  public void setExternalDatabaseVersion(String externalDatabaseVersion) {
    this.externalDatabaseVersion = externalDatabaseVersion;
  }

  public ExternalNameSource rights(String rights) {
    this.rights = rights;
    return this;
  }

   /**
   * Hints for rights/copyright
   * @return rights
  **/
  @ApiModelProperty(value = "Hints for rights/copyright")
  public String getRights() {
    return rights;
  }

  public void setRights(String rights) {
    this.rights = rights;
  }

  public ExternalNameSource externalDatabaseAuthors(String externalDatabaseAuthors) {
    this.externalDatabaseAuthors = externalDatabaseAuthors;
    return this;
  }

   /**
   * Authors of the external database/service
   * @return externalDatabaseAuthors
  **/
  @ApiModelProperty(value = "Authors of the external database/service")
  public String getExternalDatabaseAuthors() {
    return externalDatabaseAuthors;
  }

  public void setExternalDatabaseAuthors(String externalDatabaseAuthors) {
    this.externalDatabaseAuthors = externalDatabaseAuthors;
  }

  public ExternalNameSource externalDatabaseURI(String externalDatabaseURI) {
    this.externalDatabaseURI = externalDatabaseURI;
    return this;
  }

   /**
   * Link to the main page of the service/webpage
   * @return externalDatabaseURI
  **/
  @ApiModelProperty(value = "Link to the main page of the service/webpage")
  public String getExternalDatabaseURI() {
    return externalDatabaseURI;
  }

  public void setExternalDatabaseURI(String externalDatabaseURI) {
    this.externalDatabaseURI = externalDatabaseURI;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExternalNameSource externalNameSource = (ExternalNameSource) o;
    return Objects.equals(this.databaseName, externalNameSource.databaseName) &&
        Objects.equals(this.externalDatabaseID, externalNameSource.externalDatabaseID) &&
        Objects.equals(this.externalDatabaseName, externalNameSource.externalDatabaseName) &&
        Objects.equals(this.externalDatabaseVersion, externalNameSource.externalDatabaseVersion) &&
        Objects.equals(this.rights, externalNameSource.rights) &&
        Objects.equals(this.externalDatabaseAuthors, externalNameSource.externalDatabaseAuthors) &&
        Objects.equals(this.externalDatabaseURI, externalNameSource.externalDatabaseURI);
  }

  @Override
  public int hashCode() {
    return Objects.hash(databaseName, externalDatabaseID, externalDatabaseName, externalDatabaseVersion, rights, externalDatabaseAuthors, externalDatabaseURI);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExternalNameSource {\n");
    
    sb.append("    databaseName: ").append(toIndentedString(databaseName)).append("\n");
    sb.append("    externalDatabaseID: ").append(toIndentedString(externalDatabaseID)).append("\n");
    sb.append("    externalDatabaseName: ").append(toIndentedString(externalDatabaseName)).append("\n");
    sb.append("    externalDatabaseVersion: ").append(toIndentedString(externalDatabaseVersion)).append("\n");
    sb.append("    rights: ").append(toIndentedString(rights)).append("\n");
    sb.append("    externalDatabaseAuthors: ").append(toIndentedString(externalDatabaseAuthors)).append("\n");
    sb.append("    externalDatabaseURI: ").append(toIndentedString(externalDatabaseURI)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

