package org.gfbio.dwbws;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import javax.json.JsonObject;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.junit.Test;
import webservice.DWBWSserviceImpl;


public class DWBWSTest {

  @Test
  public void testIsResponding() {
    assertTrue(new DWBWSserviceImpl().isResponding());
  }

  @Test
  public void WSSearchExactTest() {
    System.out.println("*** Test search exact service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl();
    GFBioResultSet<SearchResultSetEntry> check = serviceIMPL.search("equus zebra", "exact");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSearchIncludedTest() {
    System.out.println("*** Test search included service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl();
    GFBioResultSet<SearchResultSetEntry> check = serviceIMPL.search("equus zebra", "included");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSearchCommonName() {
    System.out.println("*** Test common names search ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl();
    GFBioResultSet<SearchResultSetEntry> check = serviceIMPL.search("Gartenkreuzspinne", "exact");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermCombined() {
    System.out.println("*** Test combined term service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Plants");
    GFBioResultSet<TermCombinedResultSetEntry> check = serviceIMPL.getTermInfosCombined(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_Plants/20/");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermOriginal() {
    System.out.println("*** Test original term service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Plants");
    GFBioResultSet<TermOriginalResultSetEntry> check = serviceIMPL.getTermInfosOriginal(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_Plants/20/");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermProcessed() {
    System.out.println("*** Test processed term service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Plants", "1128");
    GFBioResultSet<SearchResultSetEntry> check = serviceIMPL.getTermInfosProcessed(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_Plants/20/");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSHierarchy() {
    System.out.println("*** Test hierarchy service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_TaxaVaria", "1143");
    GFBioResultSet<HierarchyResultSetEntry> check = serviceIMPL.getHierarchy(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_TaxaVaria/12647/");
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSallbroader() {
    System.out.println("*** Test allbroader service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Animalia", "85");
    GFBioResultSet<AllBroaderResultSetEntry> check = serviceIMPL.getAllBroader(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_Animalia/4000005");
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSynonyms() {
    System.out.println("*** Test synonym service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Plants", "1128");
    GFBioResultSet<SynonymsResultSetEntry> check = serviceIMPL.getSynonyms(
        "http://services.snsb.info/DTNtaxonlists/rest/v0.1/names/DiversityTaxonNames_Plants/23/");
    assertNotNull(check);
    System.out.println(check.create().toString());
  }

  @Test
  public void WSDatabaseMetadata() {
    System.out.println("*** Test Database Metadata service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl("DiversityTaxonNames_Animalia");
    GFBioResultSet<MetadataResultSetEntry> check = serviceIMPL.getMetadata();
    assertNotNull(check);
    for (JsonObject j : check.create()) {
      System.out.println(j.toString());
    }
  }

  @Test
  public void WSMetadata() {
    System.out.println("*** Test Metadata service ***");
    DWBWSserviceImpl serviceIMPL = new DWBWSserviceImpl();
    GFBioResultSet<MetadataResultSetEntry> check = serviceIMPL.getMetadata();
    assertNotNull(check);
    for (JsonObject j : check.create()) {
      System.out.println(j.toString());
    }
  }
}
